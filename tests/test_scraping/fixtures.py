EXEMPLE_HTML = b'<!doctype html>\n<html>\n<head>\n    <title>Example Domain</title>\n\n    <meta charset="utf-8" />\n ' \
               b'   <meta http-equiv="Content-type" content="text/html; charset=utf-8" />\n    <meta name="viewport" ' \
               b'content="width=device-width, initial-scale=1" />\n    <style type="text/css">\n    body {\n        ' \
               b'background-color: #f0f0f2;\n        margin: 0;\n        padding: 0;\n        font-family: "Open ' \
               b'Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;\n        \n    }\n    div {\n        width: ' \
               b'600px;\n        margin: 5em auto;\n        padding: 50px;\n        background-color: #fff;\n        ' \
               b'border-radius: 1em;\n    }\n    a:link, a:visited {\n        color: #38488f;\n        ' \
               b'text-decoration: none;\n    }\n    @media (max-width: 700px) {\n        body {\n            ' \
               b'background-color: #fff;\n        }\n        div {\n            width: auto;\n            margin: 0 ' \
               b'auto;\n            border-radius: 0;\n            padding: 1em;\n        }\n    }\n    </style>    ' \
               b'\n</head>\n\n<body>\n<div>\n    <h1>Example Domain</h1>\n    <p>This domain is established to be ' \
               b'used for illustrative examples in documents. You may use this\n    domain in examples without prior ' \
               b'coordination or asking for permission.</p>\n    <p><a ' \
               b'href="http://www.iana.org/domains/example">More information...</a></p>\n</div>\n</body>\n</html>\n '

BOOK_HTML = html = "<a href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729\"><img alt=\"Vacances mortelles au " \
                   "paradis par Sachs\" loading=\"lazy\" " \
                   "onerror=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" " \
                   "src=\"https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX95_.jpg\" title=\"Vacances " \
                   "mortelles au paradis par Sachs\"/><h2 itemprop=\"name\" style=\"\">Vacances mortelles au " \
                   "paradis</h2></a> "

AUTHOR_HTML = "<a href=\"/auteur/Juliette-Sachs/498755\"><h3 style=\"margin:0px\">Juliette Sachs</h3></a>"

GET_INFO_LIST = [('/livres/Sachs-Vacances-mortelles-au-paradis/1166729',
                  'Vacances mortelles au paradis',
                  'https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX95_.jpg',
                  '/auteur/Juliette-Sachs/498755',
                  'Juliette Sachs'),
                 ('/livres/Jullian-Le-berceau-du-Talion/1170670',
                  'Le berceau du Talion',
                  'https://images-eu.ssl-images-amazon.com/images/I/314zuTOFEaL._SX95_.jpg',
                  '/auteur/Sebastien-Jullian/468623',
                  'Sébastien Jullian'),
                 ('/livres/Piketty-Capital-et-ideologie/1167551',
                  'Capital et idéologie',
                  '/couv/cvt_Capital-et-ideologie_4660.jpg',
                  '/auteur/Thomas-Piketty/87205',
                  'Thomas Piketty'),
                 ('/livres/Carrisi-Le-jeu-du-Chuchoteur/1169261',
                  'Le jeu du Chuchoteur',
                  'https://images-eu.ssl-images-amazon.com/images/I/41V3alBD-PL._SX95_.jpg',
                  '/auteur/Donato-Carrisi/93832',
                  'Donato Carrisi'),
                 ('/livres/Snowden-Memoires-vives/1162306',
                  'Mémoires vives ',
                  'https://images-eu.ssl-images-amazon.com/images/I/51%2Bcx9NCv0L._SX95_.jpg',
                  '/auteur/Edward-Snowden/516723',
                  'Edward Snowden'),
                 ('/livres/Hamill-Une-cosmologie-de-monstres/1160843',
                  'Une cosmologie de monstres',
                  'https://images-eu.ssl-images-amazon.com/images/I/51LKRP9eE6L._SX95_.jpg',
                  '/auteur/Shaun-Hamill/516108',
                  'Shaun Hamill'),
                 ('/livres/Liu-Boule-de-foudre/1169042',
                  'Boule de foudre',
                  'https://images-eu.ssl-images-amazon.com/images/I/41ulQieFJ5L._SX95_.jpg',
                  '/auteur/Cixin-Liu/340977',
                  'Cixin Liu'),
                 ('/livres/Becker-La-Maison/1149677',
                  'La Maison',
                  'https://images-eu.ssl-images-amazon.com/images/I/31HSuFH8vnL._SX95_.jpg',
                  '/auteur/Emma-Becker/119509',
                  'Emma Becker'),
                 ('/livres/Chabane-Mecanique-celeste/1155575',
                  'Mécanique céleste',
                  '/couv/cvt_Mecanique-Celeste-tome-0-Mecanique-Celeste_3448.jpg',
                  '/auteur/Merwan-Chabane/139230',
                  'Merwan Chabane'),
                 ('/livres/Vanistendael-Les-deux-vies-de-Penelope/1169984',
                  'Les deux vies de Pénélope',
                  'https://images-eu.ssl-images-amazon.com/images/I/41mC6JnCkiL._SX95_.jpg',
                  '/auteur/Judith-Vanistendael/86348',
                  'Judith Vanistendael'),
                 ('/livres/Pim-Tremen/1161106',
                  'Tremen',
                  'https://images-eu.ssl-images-amazon.com/images/I/41D0smgaUvL._SX95_.jpg',
                  '/auteur/Bos-Pim/516226',
                  'Bos Pim'),
                 ('/livres/Beuglet-Lile-du-diable/1168709',
                  "L'île du diable",
                  '/couv/cvt_Lile-du-diable_7556.jpg',
                  '/auteur/Nicolas-Beuglet/405153',
                  'Nicolas Beuglet'),
                 ('/livres/Toussaint-La-cle-USB/1147662',
                  'La clé USB',
                  'https://images-eu.ssl-images-amazon.com/images/I/316RBYF%2Bs0L._SX95_.jpg',
                  '/auteur/Jean-Philippe-Toussaint/3918',
                  'Jean-Philippe Toussaint'),
                 ('/livres/Tournay-Civilisation-00/1169308',
                  'Civilisation 0.0',
                  'https://images-eu.ssl-images-amazon.com/images/I/51OuNT5PlOL._SX95_.jpg',
                  '/auteur/Virginie-Tournay/519483',
                  'Virginie Tournay'),
                 ('/livres/Crossan-Moon-brothers/1162622',
                  'Moon brothers',
                  'https://images-eu.ssl-images-amazon.com/images/I/41TekpU7JHL._SX95_.jpg',
                  '/auteur/Sarah-Crossan/368644',
                  'Sarah Crossan'),
                 ('/livres/Loubry-Les-refuges/1158683',
                  'Les refuges',
                  'https://images-eu.ssl-images-amazon.com/images/I/51MtZqxbN4L._SX95_.jpg',
                  '/auteur/Jerome-Loubry/442819',
                  'Jérôme Loubry'),
                 ('/livres/Mizubayashi-me-brisee/1151329',
                  'Âme brisée',
                  'https://images-eu.ssl-images-amazon.com/images/I/41NdIhJJM4L._SX95_.jpg',
                  '/auteur/Akira-Mizubayashi/119607',
                  'Akira Mizubayashi'),
                 ('/livres/Amigorena-Le-Ghetto-interieur/1156095',
                  'Le Ghetto intérieur',
                  'https://images-eu.ssl-images-amazon.com/images/I/41K2KM7tEVL._SX95_.jpg',
                  '/auteur/Santiago-H-Amigorena/232012',
                  'Santiago H. Amigorena'),
                 ('/livres/Tchakaloff-Vacarme/1156353',
                  'Vacarme',
                  'https://images-eu.ssl-images-amazon.com/images/I/3131dMw27cL._SX95_.jpg',
                  '/auteur/Gal-Tchakaloff/391320',
                  'Gaël Tchakaloff'),
                 ('/livres/Agrimbau-Lhumain/1158999',
                  "L'humain ",
                  'https://images-eu.ssl-images-amazon.com/images/I/51H3n3rFwtL._SX95_.jpg',
                  '/auteur/Diego-Agrimbau/106944',
                  'Diego Agrimbau'),
                 ('/livres/Altan-Je-ne-reverrai-plus-le-monde--Textes-de-prison/1152662',
                  'Je ne reverrai plus le monde : Textes de prison',
                  'https://images-eu.ssl-images-amazon.com/images/I/4155slrltML._SX95_.jpg',
                  '/auteur/Ahmet-Altan/251947',
                  'Ahmet Altan'),
                 ('/livres/McGee-American-Royals/1167325',
                  'American Royals',
                  'https://images-eu.ssl-images-amazon.com/images/I/41hcpiKyV1L._SX95_.jpg',
                  '/auteur/Katharine-McGee/418210',
                  'Katharine McGee'),
                 ('/livres/Puertolas-La-police-des-fleurs-des-arbres-et-des-forets/1158127',
                  'La police des fleurs, des arbres et des forêts',
                  'https://images-eu.ssl-images-amazon.com/images/I/51mLj8vmAfL._SX95_.jpg',
                  '/auteur/Romain-Puertolas/274608',
                  'Romain Puértolas'),
                 ('/livres/Trebor-Combien-de-pas-jusqua-la-lune-/1161604',
                  "Combien de pas jusqu'à la lune ?",
                  'https://images-eu.ssl-images-amazon.com/images/I/41ahuJtehpL._SX95_.jpg',
                  '/auteur/Carole-Trebor/201599',
                  'Carole Trebor'),
                 ('/livres/Desmurget-La-fabrique-du-cretin-digital/1167636',
                  'La fabrique du crétin digital',
                  'https://images-eu.ssl-images-amazon.com/images/I/41SJaY%2BbfsL._SX95_.jpg',
                  '/auteur/Michel-Desmurget/124235',
                  'Michel Desmurget'),
                 ('/livres/Rivault-Collisions/1155661',
                  'Collisions',
                  'https://images-eu.ssl-images-amazon.com/images/I/51i87p6eciL._SX95_.jpg',
                  '/auteur/Alexandra-Rivault/513975',
                  'Alexandra Rivault'),
                 ('/livres/Rivers-Lincivilite-des-fantomes/1158277',
                  "L'incivilité des fantômes",
                  '/couv/cvt_Lincivilite-des-fantomes_5580.jpg',
                  '/auteur/Solomon-Rivers/514938',
                  'Solomon Rivers'),
                 ('/livres/Dorison-Le-chateau-des-animaux-tome-1--Miss-Bengalore/1159801',
                  'Le château des animaux, tome 1 : Miss Bengalore',
                  '/couv/cvt_Le-chateau-des-animaux-Tome-1--Miss-Bengalore_8857.jpg',
                  '/auteur/Xavier-Dorison/2412',
                  'Xavier Dorison'),
                 ('/livres/Giraud-Jour-de-courage/1149680',
                  'Jour de courage',
                  'https://images-eu.ssl-images-amazon.com/images/I/418QsAaak6L._SX95_.jpg',
                  '/auteur/Brigitte-Giraud/3643',
                  'Brigitte Giraud'),
                 ('/livres/Bryndza-Liquide-inflammable/1155379',
                  'Liquide inflammable',
                  '/couv/cvt_Liquide-inflammable_9319.jpg',
                  '/auteur/Robert-Bryndza/442713',
                  'Robert Bryndza'),
                 ('/livres/Nicholls-Evelyn-May-et-Nell--Pour-un-monde-plus-juste/1162827',
                  'Evelyn, May et Nell : Pour un monde plus juste',
                  'https://images-eu.ssl-images-amazon.com/images/I/41CzBrwJPhL._SX95_.jpg',
                  '/auteur/Sally-Nicholls/55019',
                  'Sally Nicholls'),
                 ('/livres/Appanah-Le-ciel-par-dessus-le-toit/1147789',
                  'Le ciel par-dessus le toit',
                  'https://images-eu.ssl-images-amazon.com/images/I/41fvtDi1VuL._SX95_.jpg',
                  '/auteur/Nathacha-Appanah/19917',
                  'Nathacha Appanah'),
                 ('/livres/Lee-Destins-troubles/1161498',
                  'Destins troubles',
                  '/couv/cvt_Destins-Troubles_5925.jpg',
                  '/auteur/Geneva-Lee/393817',
                  'Geneva Lee'),
                 ('/livres/Seksik-Chaplin-en-Amerique/1166883',
                  'Chaplin en Amérique',
                  '/couv/cvt_Chaplin-en-Amerique_3882.jpg',
                  '/auteur/Laurent-Seksik/67705',
                  'Laurent Seksik'),
                 ('/livres/Cece-Les-bizarres-tome-1-Catastrophe-a-Huggabie-City/1164667',
                  'Les bizarres, tome 1: Catastrophe à Huggabie City',
                  'https://images-eu.ssl-images-amazon.com/images/I/51ryXRp0QCL._SX95_.jpg',
                  '/auteur/Adam-Cece/517700',
                  'Adam Cece'),
                 ('/livres/Arnaud-Maintenant-comme-avant/1150446',
                  'Maintenant, comme avant',
                  'https://images-eu.ssl-images-amazon.com/images/I/41yqKgOmzrL._SX95_.jpg',
                  '/auteur/Juliette-Arnaud/236708',
                  'Juliette Arnaud'),
                 ('/livres/Lopez-Barbarossa--1941-La-guerre-absolue/1168655',
                  'Barbarossa : 1941. La guerre absolue',
                  'https://images-eu.ssl-images-amazon.com/images/I/41azCGhDrQL._SX95_.jpg',
                  '/auteur/Jean-Lopez/87962',
                  'Jean Lopez'),
                 ('/livres/Bortoli-Too-Young/1167003',
                  'Too Young',
                  'https://images-eu.ssl-images-amazon.com/images/I/414AEj48y6L._SX95_.jpg',
                  '/auteur/Margot-D-Bortoli/483655',
                  'Margot D. Bortoli'),
                 ('/livres/Rebillard-Les-seins-de-Fatima-tome-1/1169110',
                  'Les seins de Fatima, tome 1',
                  '/couv/cvt_Les-seins-de-Fatima-Tome-1_8934.jpg',
                  '/auteur/Georges-Daniel-Rebillard/456011',
                  'Georges Daniel Rebillard'),
                 ('/livres/Seveno-6-ans/1167496',
                  '6 ans',
                  'https://images-eu.ssl-images-amazon.com/images/I/41CNOhHRe8L._SX95_.jpg',
                  '/auteur/Elle-Seveno/420191',
                  'Elle Seveno')]

GET_RATE_HTML = (
    "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML+RDFa 1.0//EN\" \"https://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd\">\n"
    "<html version=\"XHTML+RDFa 1.0\" xmlns=\"https://www.w3.org/1999/xhtml\" xml:lang=\"fr\">\n"
    "\n"
    "<head>\n"
    "	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n"
    "	<meta http-equiv=\"Content-language\" content=\"fr-FR\" />\n"
    "	<meta http-equiv=\"cache-control\" content=\"no-cache\">\n"
    "	<meta http-equiv=\"pragma\" content=\"no-cache\">\n"
    "	<meta http-equiv=\"expires\" content=\"-1\">\n"
    "	<link rel=\"SHORTCUT ICON\" href=\"/faviconbabelio_.ico\" />\n"
    "	<meta charset=\"iso-8859-1\">\n"
    "	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
    "	<meta name=\"viewport\" content=\"user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1\">\n"
    "	<meta property=\"og:type\" content=\"website\">\n"
    "	</meta>\n"
    "	<title>Vacances mortelles au paradis - Juliette Sachs - Babelio</title>\n"
    "	<meta name=\"title\" content=\"Vacances mortelles au paradis - Juliette Sachs - Babelio\" />\n"
    "	<meta name=\"description\"\n"
    "content=\"Critiques (7), citations (2), extraits de Vacances mortelles au paradis de Juliette Sachs. Difficile "
    "de passer d'un tr�s bon polar de Micha�l Connelly � celui-ci...\" />\n "
    "	<meta name=\"keywords\"\n"
    "		content=\"romans policiers et polars,humour,cosy mystery,fran�ais,suspense,com�die,meurtre,�les,_\" />\n"
    "	<meta name=\"twitter:card\" content=\"summary\">\n"
    "	<meta name=\"twitter:title\" content=\"Vacances mortelles au paradis - Juliette Sachs - Babelio\">\n"
    "	<meta name=\"twitter:description\"\n"
    "content=\"Critiques (7), citations (2), extraits de Vacances mortelles au paradis de Juliette Sachs. Difficile "
    "de passer d'un tr�s bon polar de Micha�l Connelly � celui-ci...\">\n "
    "	<meta name=\"twitter:domain\" content=\"https://www.babelio.com\">\n"
    "	<meta property=\"og:title\" content=\"Vacances mortelles au paradis - Juliette Sachs - Babelio\" />\n"
    "	<meta property=\"og:description\"\n"
    "content=\"Critiques (7), citations (2), extraits de Vacances mortelles au paradis de Juliette Sachs. Difficile "
    "de passer d'un tr�s bon polar de Micha�l Connelly � celui-ci...\" />\n "
    "	<link rel=\"stylesheet\" type=\"text/css\" href=\"/css_cache/17,18,20,21__45.css\" media=\"all\" />\n"
    "	<link rel=\"image_src\" href=\"https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX195_.jpg\" />\n"
    "<meta property=\"og:image\" content=\"https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX195_.jpg"
    "\">\n "
    "<meta name=\"twitter:image:src\" content=\"https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX195_"
    ".jpg\">\n "
    "	<link rel=\"canonical\" href=\"https://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis/1166729\" />\n"
    "<meta property=\"og:url\" content=\"https://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis/1166729"
    "\">\n "
    "	<meta property=\"fb:app_id\" content=\"40153783332\" />\n"
    "	<meta property=\"og:title\" content=\"Vacances mortelles au paradis - Juliette Sachs\" />\n"
    "	<meta property=\"og:type\" content=\"book\" />\n"
    "<meta property=\"og:url\" content=\"https://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis/1166729\" "
    "/>\n "
    "<meta property=\"og:image\" content=\"https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX195_.jpg\" "
    "/>\n "
    "	<meta property=\"og:description\"\n"
    "content=\"Critiques (7), citations (2), extraits de Vacances mortelles au paradis de Juliette Sachs. Difficile "
    "de passer d'un tr�s bon polar de Micha�l Connelly � celui-ci...\" />\n "
    "	<meta property=\"og:site_name\" content=\"Babelio\" />\n"
    "	<meta property=\"og:locale\" content=\"fr_FR\" />\n"
    "	<script type='text/javascript'>\n"
    "		var yieldlove_site_id = \"babelio.com_deconnecte\";\n"
    "	</script>\n"
    "	<script type='text/javascript' src='//cdn-a.yieldlove.com/yieldlove-bidder.js?babelio.com_deconnecte'></script>\n"
    "	<script type=\"text/javascript\">\n"
    "		var habillage_state = 0;\n"
    "		var googletag = googletag || {};\n"
    "		googletag.cmd = googletag.cmd || [];\n"
    "		(function () {\n"
    "			var gads = document.createElement(\"script\");\n"
    "			gads.async = true;\n"
    "			gads.type = \"text/javascript\";\n"
    "			var useSSL = \"https:\" == document.location.protocol;\n"
    "			gads.src = (useSSL ? \"https:\" : \"http:\") + \"//www.googletagservices.com/tag/js/gpt.js\";\n"
    "			var node = document.getElementsByTagName(\"script\")[0];\n"
    "			node.parentNode.insertBefore(gads, node);\n"
    "		})();\n"
    "	</script>\n"
    "	<script type='text/javascript'>\n"
    "		googletag.cmd.push(function () {\n"
    "\n"
    "			googletag.pubads().setTargeting('habillage', \"2\");\n"
    "googletag.pubads().setTargeting('campagnes_pub_params', [\"\", \"litterature\", \"jeunesse\", \"littfr\", "
    "\"polar\", \"polarthriller\", \"adowestern\", \"famillej\", \"chardonneret\", \"teule\", \"simples\", "
    "\"khalil\", \"chandra\", \"amourromance\", \"couple\"]);\n "
    "			googletag.pubads().setTargeting('home', \"\");\n"
    "\n"
    "			/*\n"
    "			var mapping_habillage = googletag.sizeMapping().\n"
    "				addSize([0, 0], []).\n"
    "			  addSize([480, 0], [480,190]). // tel\n"
    "			  addSize([768, 0], [768,190]). // Tablette\n"
    "			  addSize([1024, 0],  [0,0]). // Ordinateur de bureau\n"
    "			  build();\n"
    "			    \n"
    "				googletag.defineOutOfPageSlot('/1032445/habillage_site', 'div-gpt-ad-1442608793107-0').\n"
    "					  defineSizeMapping(mapping_habillage).\n"
    "						setCollapseEmptyDiv(true).  \n"
    "				addService(googletag.pubads()); \n"
    "			   \n"
    "			   */\n"
    "\n"
    "googletag.defineOutOfPageSlot('/1032445/habillage_site', 'div-gpt-ad-1442608793107-0').addService("
    "googletag.pubads());\n "
    "\n"
    "\n"
    "			var mapping_meg_haut = googletag.sizeMapping().\n"
    "				addSize([0, 0], [[300, 250], [320, 100]]).\n"
    "				addSize([480, 0], [320, 100]). // tel\n"
    "				addSize([768, 0], [728, 90]). // Tablette\n"
    "				addSize([1024, 0], [[728, 90], [970, 90], [1000, 90]]). // Ordinateur de bureau\n"
    "				build();\n"
    "\n"
    "googletag.defineSlot('/1032445/megabann_haut', [[320, 100], [728, 90], [970, 90], [1000, 90]], "
    "'div-gpt-ad-1442725206084-0').\n "
    "				defineSizeMapping(mapping_meg_haut).\n"
    "				addService(googletag.pubads());\n"
    "\n"
    "\n"
    "			var mapping_pave_bas = googletag.sizeMapping().\n"
    "				addSize([0, 0], [300, 250]).\n"
    "				addSize([1024, 0], [[300, 600], [300, 250]]). // Ordinateur de bureau\n"
    "				build();\n"
    "\n"
    "			googletag.defineSlot('/1032445/pave_bas', [[300, 600], [300, 250]], 'div-gpt-ad-1442725480364-1').\n"
    "				defineSizeMapping(mapping_pave_bas).\n"
    "				setCollapseEmptyDiv(true).\n"
    "				addService(googletag.pubads());\n"
    "\n"
    "\n"
    "			var mapping_meg_bas = googletag.sizeMapping().\n"
    "				addSize([0, 0], [[300, 250], [320, 100]]).\n"
    "				addSize([480, 0], [[300, 250], [320, 100]]). // tel\n"
    "				addSize([768, 0], [728, 90]). // Tablette\n"
    "				addSize([1024, 0], [[728, 90], [970, 90]]). // Ordinateur de bureau\n"
    "				build();\n"
    "\n"
    "			googletag.defineSlot('/1032445/megabann_bas', [728, 90], 'div-gpt-ad-1442725480364-0').\n"
    "				defineSizeMapping(mapping_meg_bas).\n"
    "				setCollapseEmptyDiv(true).\n"
    "				addService(googletag.pubads());\n"
    "\n"
    "\n"
    "			var mapping_pave_haut = googletag.sizeMapping().\n"
    "				addSize([0, 0], [300, 250]).\n"
    "				addSize([1024, 0], [[300, 600], [300, 250]]). // Ordinateur de bureau\n"
    "				build();\n"
    "\n"
    "			googletag.defineSlot('/1032445/pave_haut', [[300, 600], [300, 250]], 'div-gpt-ad-1442725480364-2').\n"
    "				defineSizeMapping(mapping_pave_haut).\n"
    "				setCollapseEmptyDiv(true).\n"
    "				addService(googletag.pubads());\n"
    "\n"
    "			var mapping_test_mobile = googletag.sizeMapping().\n"
    "				addSize([0, 0], [320, 50]). // Mobile\n"
    "				addSize([480, 0], [320, 250]). // Tablette\n"
    "				addSize([1024, 0], []). // Ordinateur de bureau\n"
    "				build();\n"
    "\n"
    "\n"
    "			googletag.defineSlot('/1032445/test_mobile', [320, 50], 'div-gpt-ad-1569254069434-0').\n"
    "				defineSizeMapping(mapping_test_mobile).\n"
    "				setCollapseEmptyDiv(true).\n"
    "				addService(googletag.pubads());\n"
    "\n"
    "\n"
    "\n"
    "			var mapping_pave_responsive_haut = googletag.sizeMapping().\n"
    "				addSize([0, 0], [[320, 100], [300, 250]]).\n"
    "				addSize([480, 0], [300, 250]). // Tablette\n"
    "				addSize([1024, 0], []). // Ordinateur de bureau\n"
    "				build();\n"
    "\n"
    "googletag.defineSlot('/1032445/pave_responsive_haut', [[300, 250], [300, 100]], 'div-gpt-ad-1465403049525-0').\n "
    "				defineSizeMapping(mapping_pave_responsive_haut).\n"
    "				setCollapseEmptyDiv(true).\n"
    "				addService(googletag.pubads());\n"
    "\n"
    "			googletag.pubads().enableSingleRequest();\n"
    "			googletag.pubads().collapseEmptyDivs();\n"
    "			//googletag.pubads().enableSyncRendering();\n"
    "			googletag.enableServices();\n"
    "			googletag.pubads().addEventListener('slotRenderEnded', function (event) {\n"
    "				if (event.slot.getSlotElementId() == \"div-gpt-ad-1442608793107-0\") {\n"
    "					if (!event.isEmpty) {\n"
    "						SetHabillage();\n"
    "					}\n"
    "				}\n"
    "\n"
    "			});\n"
    "\n"
    "		});\n"
    "	</script>\n"
    "	<link rel=\"dns-prefetch\" href=\"//fonts.googleapis.com\">\n"
    "	<link rel=\"dns-prefetch\" href=\"https://connect.facebook.net\">\n"
    "	<link rel=\"prefetch\" href=\"/images/spritesheet.png\">\n"
    "	<link rel=\"dns-prefetch\" href=\"//google-analytics.com\">\n"
    "	<link rel=\"dns-prefetch\" href=\"//www.google-analytics.com\">\n"
    "	<link rel=\"dns-prefetch\" href=\"http://www.googletagservices.com\">\n"
    "	<link rel=\"dns-prefetch\" href=\"http://partner.googleadservices.com\">\n"
    "</head>\n"
    "\n"
    "<body onunload=\"\" itemscope itemtype=\"http://schema.org/WebPage\">\n"
    "	<div id='div-gpt-ad-1442608793107-0' style=\"z-index:1;\">\n"
    "		<script type='text/javascript'>\n"
    "			googletag.cmd.push(function () { googletag.display('div-gpt-ad-1442608793107-0'); });\n"
    "		</script>\n"
    "	</div><input type=\"hidden\" id=\"hid_version\" value=\"3\">\n"
    "	<div id=\"overlay\"></div>\n"
    "	<div id=\"lightbox_overlay\"></div>\n"
    "	<div id=\"lightbox\">\n"
    "		<div id=\"lightbox_close\" class=\"tiny_links dark\">fermer</div>\n"
    "		<div id=\"lightbox_con\"></div>\n"
    "	</div>\n"
    "<div id=\"mobile_menu\"><a href=\"/actualites.php\" rel=\"nofollow\" id=\"bleb4i\">Accueil</a><a "
    "href=\"/mabibliotheque.php\"\n "
    "			rel=\"nofollow\">Mes livres</a><a href=\"/ajoutlivres.php\" rel=\"nofollow\">Ajouter des livres</a>\n"
    "		<div class=\"sep\"></div>\n"
    "		<div class=\"titre\">D�couvrir</div><a href=\"/decouvrir.php\">Livres</a><a\n"
    "			href=\"/decouvrirauteurs.php\">Auteurs</a><a href=\"/decouvrirmembres.php\">Lecteurs</a><a\n"
    "			href=\"/dernierescritiques.php\">Critiques</a><a href=\"/dernierescitations.php\">Citations</a><a\n"
    "			href=\"/listes-de-livres/\">Listes</a><a href=\"/quiz/\">Quiz</a><a href=\"/groupes\"\n"
    "			rel=\"no-follow\">Groupes</a><a href=\"/questions\" rel=\"no-follow\">Questions</a><a href=\"/prix-babelio\"\n"
    "			rel=\"no-follow\">Prix Babelio</a>\n"
    "	</div>\n"
    "	<div id=\"mobile_user_menu\"></div>\n"
    "	<style media=\"screen\" type=\"text/css\">\n"
    "		@media (min-width: 1025px) {\n"
    "			.logo_svg {\n"
    "				width: 171px;\n"
    "				margin: 25px 15px 20px 10px;\n"
    "				float: left;\n"
    "\n"
    "			}\n"
    "\n"
    "			header .menu .menu_item_decouvrir_con {\n"
    "				margin-left: 5px;\n"
    "			}\n"
    "		}\n"
    "\n"
    "		@media (max-width: 1024px) {\n"
    "			.logo_svg {\n"
    "				margin: 10px 15px 10px 0;\n"
    "				width: 110px;\n"
    "				height: 41px;\n"
    "				float: left;\n"
    "			}\n"
    "		}\n"
    "	</style>\n"
    "	<div id=\"wrapper\" style=\"\">\n"
    "		<div class=\"top_facebook only-desktop\" id=\"bandeau_incitation\">\n"
    "			Rejoignez Babelio pour d�couvrir vos prochaines lectures <a href=\"/facebook\" rel=\"nofollow\"\n"
    "				class=\"top_facebook_login\">connexion avec <div></div></a><a href=\"/register.php\" rel=\"nofollow\"\n"
    "				class=\"top_facebook_signin\">Inscription Classique</a></div>\n"
    "		<header class=\"row\" id=\"page_head\">\n"
    "<div id=\"mobile_menu_btn\"><span class=\"row1\"></span><span class=\"row2\"></span><span "
    "class=\"row3\"></span>\n "
    "			</div><a href=\"/\"><img src=\"/images/logo-babelio-header.png\" alt=\"Babelio\" srcset=\"/images/logo.svg\"\n"
    "					class=\"logo_svg\"></a>\n"
    "			<div class=\"header_search\">\n"
    "				<form enctype=\"multipart/form-data\" action=\"/resrecherche.php\" method=\"post\" id=\"form2\"><input\n"
    "						type=\"text\" class=\"input\" placeholder=\"Rechercher\" name=\"Recherche\" id=\"searchbox\" /><button\n"
    "						type=\"submit\" name=\"recherche\" class=\"header_search_submit icon-blogger8\"></button></form><a\n"
    "					href=\"/recherche_avancee.php\" class=\"header_search_more\">plus d'options</a>\n"
    "				<div id=\"header_search_close\" class=\" icon-blogger9\"></div>\n"
    "			</div>\n"
    "			<div class=\"menu\">\n"
    "				<div class=\"menu_item menu_item_decouvrir_con \">\n"
    "					<div id=\"menu_item_decouvrir_under\" class=\"menu_under\"><a href=\"/decouvrir.php\">Livres</a><a\n"
    "							href=\"/decouvrirauteurs.php\">Auteurs</a><a href=\"/decouvrirmembres.php\">Lecteurs</a><a\n"
    "							href=\"/dernierescritiques.php\">Critiques</a><a\n"
    "							href=\"/dernierescitations.php\">Citations</a><a href=\"/listes-de-livres/\">Listes</a><a\n"
    "							href=\"/quiz/\">Quiz</a><a href=\"/groupes\" rel=\"no-follow\">Groupes</a><a href=\"/questions\"\n"
    "							rel=\"no-follow\">Questions</a><a href=\"/prix-babelio\" rel=\"no-follow\">Prix Babelio</a></div>\n"
    "					<a href=\"/decouvrir.php\" id=\"menu_item_decouvrir\" class=\" menu_link\">\n"
    "						D�couvrir\n"
    "						<div id=\"menu_item_decouvrir_arrow_1\" class=\"icon-blogger7\"></div>\n"
    "						<div id=\"menu_item_decouvrir_arrow_2\" class=\"icon-blogger7\"></div>\n"
    "					</a>\n"
    "				</div>\n"
    "				<div class=\"menu_item_anim\">\n"
    "					<div class=\"menu_item_sep\"></div><a href=\"/actualites.php\" rel=\"nofollow\"\n"
    "						class=\"menu_item menu_link\">Accueil<span></span></a><a href=\"/mabibliotheque.php\" rel=\"nofollow\"\n"
    "						class=\"menu_item menu_link\">Mes livres<span></span></a><a href=\"/ajoutlivres.php\" rel=\"nofollow\"\n"
    "						class=\"menu_item menu_link\">Ajouter des livres<span></span></a>\n"
    "				</div>\n"
    "				<div class=\"menu_sep\"></div>\n"
    "				<div class=\"menu_compte\"><br>\n"
    "					<table>\n"
    "						<tr>\n"
    "							<td><a href=\"/facebook\"\n"
    "style=\"background: url('/images/spritesheet.png') no-repeat scroll 0px -0px;    color: #FFF;    padding: 0px;   "
    " display: block;width:20px;height:20px\"\n "
    "									;></a></td>\n"
    "							<td><a href=\"/connection.php\" rel=\"nofollow\"><b>Connexion</b></a></td>\n"
    "						</tr>\n"
    "					</table><a href=\"/recupmdp.php\" rel=\"nofollow\" style=\"font-size: 0.5rem;\">mot de passe oubli� ?</a>\n"
    "				</div>\n"
    "			</div>\n"
    "			<div id=\"mobile_user_btn\"><a href=\"/connection.php\" rel=\"nofollow\">\n"
    "					<div class=\"icon-enter\"></div>\n"
    "				</a></div>\n"
    "			<div id=\"mobile_search_btn\" class=\" icon-blogger8\"></div>\n"
    "		</header>\n"
    "		<div class=\"corps row\" id=\"page_corps\" style=\"z-index:2;position:relative;\">\n"
    "			<div class=\"content row\">\n"
    "				<div class=\"megaban\" id=\"megaban\">\n"
    "					<div id='div-gpt-ad-1442725206084-0'>\n"
    "						<script type='text/javascript'>\n"
    "							googletag.cmd.push(function () { googletag.display('div-gpt-ad-1442725206084-0'); });\n"
    "						</script>\n"
    "					</div>\n"
    "				</div>\n"
    "				<div itemscope itemtype=\"https://schema.org/Book\">\n"
    "					<div class=\"livre_header row\">\n"
    "						<table>\n"
    "							<tr>\n"
    "								<td style=\"width: 1px; white-space: nowrap;\"><a\n"
    "										href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729\"\n"
    "										class=\"only-desktop\"><img\n"
    "											src=\"https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX195_.jpg\"\n"
    "											alt=\"Vacances mortelles au paradis par Juliette Sachs\"\n"
    "											title=\"Vacances mortelles au paradis par Juliette Sachs\"\n"
    "											onError=\"this.onerror=null;this.src='/images/couv-defaut-grande.jpg';\" /></a>\n"
    "								</td>\n"
    "								<td style=\"min:height:15em;\">\n"
    "									<div class=\"livre_header_con\">\n"
    "										<h1 itemprop=\"name\"><a\n"
    "												href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729\">\n"
    "												Vacances mortelles au paradis\n"
    "											</a></h1>\n"
    "										<div class=\"livre_header_links menu_item_anim\"><a\n"
    "												href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729\"\n"
    "												class=\"menu_link current\">infos<span></span></a><a rel=\"subsection\"\n"
    "												href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques\"\n"
    "												class=\"menu_link\">Critiques (7)<span></span></a><a\n"
    "												href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729#citations\"\n"
    "												class=\"menu_link\">Citations (2)<span></span></a><a\n"
    "												href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/forum\"\n"
    "												class=\"menu_link\">\n"
    "												Forum\n"
    "												<span></span></a></div>\n"
    "									</div>\n"
    "								</td>\n"
    "							</tr>\n"
    "						</table>\n"
    "					</div>\n"
    "					<div class=\"side_l\">\n"
    "						<div class=\"livre_con\">\n"
    "							<div class=\"col col-4\"><img\n"
    "									src=\"https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX195_.jpg\"\n"
    "									alt=\"Vacances mortelles au paradis par Sachs\"\n"
    "									title=\"Vacances mortelles au paradis par Juliette Sachs\" class=\"\"\n"
    "									style=\"max-width:210px;\"\n"
    "									onError=\"this.onerror=null;this.src='/images/couv-defaut-grande.jpg';\" /><br><a\n"
    "									class=\"btn btn_yellow\" href=\"/connection.php\">Ajouter � mes livres</a><br><br><a\n"
    "									href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/extraits\"\n"
    "									class=\"tiny_links btn_icon\" style=\"width: 200px;\"><span class=\"icon-book\"\n"
    "										style=\"background-color: #393939;\"></span>\n"
    "									<div>Lire un extrait</div>\n"
    "								</a></div>\n"
    "							<div class=\"col col-8\"><span itemprop=\"author\" itemscope=\"\"\n"
    "									itemtype=\"https://schema.org/Person\"><a href=\"/auteur/Juliette-Sachs/498755\"\n"
    "										itemprop=\"url\" class=\"livre_auteurs\"><span itemprop=\"name\">Juliette\n"
    "											<b>Sachs</b></span></a></span>\n"
    "								<div class=\"livre_refs grey_light\">\n"
    "									ISBN : 2824615419 <br />\n"
    "									�diteur : <a href=\"/editeur/1234/City-Editions\" class=\"tiny_links dark\">City\n"
    "										Editions</a>\n"
    "									(04/09/2019)\n"
    "\n"
    "\n"
    "								</div><br><span itemprop=\"aggregateRating\" itemscope\n"
    "									itemtype=\"https://schema.org/AggregateRating\">\n"
    "									Note moyenne : <span class=\"texte_t2 rating\"\n"
    "										itemprop=\"ratingValue\">4.23</span>/<span itemprop=\"bestRating\">5</span> (sur\n"
    "									<span itemprop=\"ratingCount\">13</span> notes)\n"
    "\n"
    "									<meta itemprop=\"worstRating\" content=\"0\" />\n"
    "									<meta itemprop=\"reviewCount\" content=\"7\" /><span class=\"item\"\n"
    "										style=\"display:none\"><span class=\"fn\">Vacances mortelles au\n"
    "											paradis</span></span></span>\n"
    "								<div class=\"livre_resume_top row\">\n"
    "									R�sum� :<span></span></div>\n"
    "								<div id=\"d_bio\" style=\"margin:5px;\" itemprop=\"description\" class=\"livre_resume\">\n"
    "									Une semaine aux Maldives tous frais pay�s, c�est le r�ve ! Mais pour Alice, ce\n"
    "									s�jour n�a rien de paradisiaque... Cette c�libataire qui fr�le la quarantaine et\n"
    "									aime un peu trop les mojitos, se retrouve coinc�e avec sa famille sur une �le\n"
    "									minuscule pour y c�l�brer le mariage de sa s�ur. Entre sa m�re qui cherche � tout\n"
    "									prix � lui trouver un fianc� et un p�re qui retombe en adolescence avec sa nouvelle\n"
    "									femme de vingt-cinq ans, Alice est � bout. Et quand l�une des invi... <span\n"
    "										style=\"color:#FF9D38;\">><a href=\"javascript:void(0);\" class=\"\"\n"
    "											onclick=\"javascript:voir_plus_a('#d_bio',1,976171);\"\n"
    "											style=\"color:#FF9D38;\">Voir plus</a></span>\n"
    "									<p class=\"footer\" style=\"clear:both;border:0px;\"></p>\n"
    "								</div>\n"
    "							</div>\n"
    "							<div class=\"livre_buttons row\"><a href=\"/connection.php\" rel=\"nofollow\" rel=\"nofollow\"\n"
    "									class=\"tiny_links btn_icon \"><span class=\"icon-quote\"></span>\n"
    "									<div>Ajouter une citation</div>\n"
    "								</a><a href=\"/connection.php\" rel=\"nofollow\" rel=\"nofollow\"\n"
    "									class=\"tiny_links btn_icon \"><span class=\"icon-write\"></span>\n"
    "									<div>Ajouter une critique</div>\n"
    "								</a></div>\n"
    "						</div>\n"
    "						<div class=\"no-desktop\" style=\"clear:left;\">\n"
    "							<style media=\"screen\" type=\"text/css\">\n"
    "								.piclib:hover {\n"
    "									opacity: 0.6;\n"
    "									filter: alpha(opacity=60);\n"
    "									/* For IE8 and earlier */\n"
    "								}\n"
    "							</style>\n"
    "							<div class=\"side_r_content\">\n"
    "								<div class=\"titre\">\n"
    "									Acheter ce livre sur\n"
    "								</div><br>\n"
    "								<table>\n"
    "									<tr>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=e4474f47b8229372545311f36b239b7aHR0cDovL2V1bHRlY2guZm5hYy5jb20vZHluY2xpY2svZm5hYy8"
    "/ZXB0LXB1Ymxpc2hlcj1CYWJlbGlvJmVwdC1uYW1lPWdlbmVyaXF1ZSZldXJsPWh0dHA6Ly9yZWNoZXJjaGUuZm5hYy5jb20vU2VhcmNoUmVzdWx0L"
    "1Jlc3VsdExpc3QuYXNweD9TQ2F0PTIlMjExJlNlYXJjaD05NzgyODI0NjE1NDE3JnNmdD0xJnNhPTAmT3JpZ2luPVBBX0JBQkVMSU8=\"\n "
    "												target=\"_blank\" rel=\"nofollow\"><img src=\"/users/fnac2.jpg\" title=\"Fnac\"\n"
    "													alt=\"Fnac\" class=\"piclib\" style=\"width:62px;\" /></a></td>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=06ff2ecf809873925c52fc60b76a986aHR0cDovL3d3dy5hbWF6b24uZnIvZ3AvcHJvZHVjdC8yODI0NjE1NDE5P2llPVVURjgmdGFnPWJhYmVsaW"
    "8tMjEmbGlua0NvZGU9YXMyJmNhbXA9MTY0MiZjcmVhdGl2ZT02NzQ2JmNyZWF0aXZlQVNJTj0yODI0NjE1NDE5\"\n "
    "												target=\"_blank\" rel=\"nofollow\"><img src=\"/users/amazon2.jpg\"\n"
    "													title=\"Amazon\" alt=\"Amazon\" class=\"piclib\"\n"
    "													style=\"width:62px;\" /></a></td>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=c95ae6f631c243fa4040358626dde60aHR0cDovL3RyYWNrLmVmZmlsaWF0aW9uLmNvbS9zZXJ2bGV0L2VmZmkucmVkaXI"
    "/aWRfY29tcHRldXI9MTM0MTE2MjQmdXJsPWh0dHA6Ly93d3cucHJpY2VtaW5pc3Rlci5jb20vcy85NzgyODI0NjE1NDE3\"\n "
    "												target=\"_blank\" rel=\"nofollow\"><img src=\"/users/logorakuten2.png\"\n"
    "													title=\"Rakuten\" alt=\"Rakuten\" class=\"piclib\"\n"
    "													style=\"width:62px;\" /></a></td>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=55ebe2a3cc55fce3d05cb88d6bd35c1aHR0cHM6Ly93d3cuY3VsdHVyYS5jb20vY2F0YWxvZ3NlYXJjaC9yZXN1bHQvP3E9OTc4MjgyNDYxNTQxNz"
    "91dG1fc291cmNlPWJhYmVsaW8lMjZ1dG1fbWVkaXVtPWFmZmlsaWF0aW9uJTI2dXRtX2NhbXBhaWduPWFmZmluaXRhaXJlI2FlODQ2\"\n "
    "												target=\"_blank\" rel=\"nofollow\"><img src=\"/users/aff-cultura.jpg\"\n"
    "													title=\"Cultura\" alt=\"Cultura\" class=\"piclib\"\n"
    "													style=\"width:62px;\" /></a></td>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=e5340d415f4218c4d35309cba8b104caHR0cHM6Ly93d3cuYXdpbjEuY29tL2NyZWFkLnBocD9hd2lubWlkPTc0ODEmYXdpbmFmZmlkPTQ5MTAyOS"
    "ZjbGlja3JlZj0mcD1odHRwcyUzQSUyRiUyRnQubmVvcnktdG0ubmV0JTJGdG0lMkZhJTJGY2hhbm5lbCUyRnRyYWNrZXIlMkZlYTJjYjE0ZTQ4JTNG"
    "dG1yZGUlM0RodHRwcyUzQSUyRiUyRnd3dy5tb21veC1zaG9wLmZyJTJGcHJvZHVpdHMtQzAlMkYlM0ZzZWFyY2hwYXJhbSUzRDk3ODI4MjQ2MTU0MT"
    "c=\"\n "
    "												target=\"_blank\" rel=\"nofollow\"><img src=\"/users/momox2.jpg\"\n"
    "													title=\"Momox\" alt=\"Momox\" class=\"piclib\" style=\"width:62px;\" /></a>\n"
    "										</td>\n"
    "									</tr>\n"
    "								</table>\n"
    "							</div>\n"
    "						</div>\n"
    "						<div id='div-gpt-ad-1465403049525-0' style=\"text-align:center;padding-bottom:15px;\">\n"
    "							<script type='text/javascript'>\n"
    "								googletag.cmd.push(function () { googletag.display('div-gpt-ad-1465403049525-0'); });\n"
    "							</script>\n"
    "						</div>\n"
    "						<div class=\"titre\">\n"
    "							�tiquettes\n"
    "\n"
    "\n"
    "							<a href=\"/connection.php\" rel=\"nofollow\" class=\"tiny_links only-desktop\">\n"
    "\n"
    "								Ajouter des �tiquettes</a></div>\n"
    "						<div class=\"side_l_content\">\n"
    "							<p class=\"tags\"><a rel=\"tag\" class=\"tag_t14 tc_0 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/roman/1\"> roman </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_0 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/comedie/441\"> com�die </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_0 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/suspense/154\"> suspense </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/liberte/547\"> libert� </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/police/3308\"> police </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/frisson/4555\"> frisson </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/celibataire/21734\"> c�libataire </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/vacances/5198\"> vacances </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/iles/16757\"> �les </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t19 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/cosy-mystery/181308\"> cosy mystery </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t23 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/humour/15\"> humour </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/romance/549\"> romance </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/mariage/3682\"> mariage </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/adolescence/191\"> adolescence </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/meurtre/505\"> meurtre </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/meurtrier/5604\"> meurtrier </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/assassin/5669\"> assassin </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t26 tc_1 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/romans-policiers-et-polars/63883\"> romans policiers et polars </a>\n"
    "								&nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t14 tc_2 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/Maldives/88625\"> Maldives </a> &nbsp;\n"
    "								<a rel=\"tag\" class=\"tag_t19 tc_2 tc_noreco00 tc_noaff00 tc_bold \"\n"
    "									href=\"/livres-/francais/19\"> fran�ais </a> &nbsp;\n"
    "							</p>\n"
    "						</div>\n"
    "						<div class=\"titre\" id=\"critiques\">\n"
    "							Critiques, Analyses et Avis (7)\n"
    "							<a rel=\"subsection\" href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques\"\n"
    "								class=\"more\"><span></span>Voir plus</a><a href=\"/connection.php\" rel=\"nofollow\"\n"
    "								class=\"tiny_links only-desktop\">\n"
    "\n"
    "								Ajouter une critique</a></div>\n"
    "						<div class=\"side_l_content\">\n"
    "							<style media=\"screen\" type=\"text/css\">\n"
    "								.ratingblock {\n"
    "									float: right;\n"
    "									margin-right: 9px;\n"
    "								}\n"
    "							</style>\n"
    "							<div class=\"post\" itemscope itemtype=\"http://schema.org/Review\"><span\n"
    "									itemprop=\"itemReviewed\" itemscope itemtype=\"http://schema.org/Thing\">\n"
    "									<meta itemprop=\"name\" content=\"Vacances mortelles au paradis\" />\n"
    "									<meta itemprop=\"url\"\n"
    "content=\"http://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2017498\" />\n "
    "									<div class=\"post_con\" id=\"B_CRI2017498\">\n"
    "										<table>\n"
    "											<tr>\n"
    "												<td class=\"post_avatar not-mobile\"><a\n"
    "														href=\"/monprofil.php?id_user=162201\" rel=\"nofollow\"\n"
    "														class=\"not-mobile\"><img src=\"/users/162201_a6067.jpeg\"\n"
    "															alt=\"Binchy\" class=\"not-mobile\" /></a></td>\n"
    "												<td>\n"
    "													<table style=\"width: 95%;\">\n"
    "														<tr>\n"
    "															<td class=\"no_img\"><a href=\"/monprofil.php?id_user=162201\"\n"
    "																	class=\"author\" rel=\"nofollow\"><span\n"
    "																		itemprop=\"author\" itemscope\n"
    "																		itemtype=\"http://schema.org/Thing\"><span\n"
    "																			itemprop=\"name\">Binchy</span>\n"
    "																		<meta itemprop=\"sameAs\"\n"
    "																			content=\"http://www.babelio.com/monprofil.php?id_user=162201\">\n"
    "																		</span></a> &nbsp;\n"
    "																<span class=\"gris\"\n"
    "																	style='font-family: \"Open Sans\",sans-serif;'>20\n"
    "																	septembre 2019</span><br></td>\n"
    "															<td style=\"text-align:right;\">\n"
    "																<div data-id=\"\" class=\"rateit\" data-rateit-mode=\"font\"\n"
    "																	data-rateit-value=\"5.0\" data-rateit-ispreset=\"true\"\n"
    "																	data-rateit-readonly=\"true\"\n"
    "																	data-rateit-resetable=\"false\"\n"
    "																	data-rateit-icon=\"&#9733;\" data-test-rateit=\"Else\">\n"
    "																</div><span itemprop=\"reviewRating\" itemscope\n"
    "																	itemtype=\"http://schema.org/Rating\">\n"
    "																	<meta itemprop=\"worstRating\" content=\"1\" />\n"
    "																	<meta itemprop=\"ratingValue\" content=\"5.0\" />\n"
    "																	<meta itemprop=\"bestRating\" content=\"5\" /></span>\n"
    "															</td>\n"
    "															<td>\n"
    "																<div class=\"dropdown\">\n"
    "																	<div class=\"icon-blogger7\"></div>\n"
    "																	<div class=\"dropdown_open\"><a\n"
    "																			href=\"/signaler_abus.php?id_item=2017498&type=1\"\n"
    "																			rel=\"nofollow\">Signaler ce contenu</a><a\n"
    "																			href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2017498\">Voir\n"
    "																			la page de la critique</a></div>\n"
    "																</div>\n"
    "															</td>\n"
    "														</tr>\n"
    "													</table>\n"
    "													<div class=\"text row\">\n"
    "														<div id=\"cri_2017498\" style=\"height:10.5em;overflow:hidden;\"><a\n"
    "																href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729\"\n"
    "																class=\"titre1\">Vacances mortelles au Paradis</a> - <a\n"
    "																href=\"/auteur/Juliette-Sachs/498755\"\n"
    "																class=\"libelle\">Juliette Sachs</a> : dans votre\n"
    "															librairie ind�pendante depuis le 4 septembre dernier\n"
    "															!<br />\"Un frisson me traverse l'�chine. Un meurtre ? Dans\n"
    "															ce lieu si paradisiaque ? Cela semble contre\n"
    "															nature\".<br />Une semaine aux Maldives tous frais pay�s,\n"
    "															c'est le r�ve ! Mais pour Alice, ce s�jour n'a rien de\n"
    "															paradisiaque... Cette c�libataire qui fr�le la quarantaine\n"
    "															et aime un peu trop les mojitos, se retrouve coinc�e avec sa\n"
    "															famille sur une �le minuscule pour y c�l�brer le mariage de\n"
    "															sa soeur.<br />Entre sa m�re qui cherche � tout prix � lui\n"
    "															trouver un fianc� et un p�re qui retombe en adolescence avec\n"
    "															sa nouvelle femme de vingt-cinq ans, Alice est � bout. Et\n"
    "															quand l'une des invit�es est retrouv�e assassin�e dans sa\n"
    "															chambre, c'est vraiment le pompon ! D'autant que tout le\n"
    "															monde a interdiction de quitter l'�le tant que le meurtrier\n"
    "															est en libert�.<br />Face � l'incomp�tence de la police\n"
    "															locale, Alice d�cide de prendre le taureau par les cornes et\n"
    "															de d�masquer elle-m�me l'assassin. Plus facile � dire qu'�\n"
    "															faire, car parmi la centaine d'invit�s, ils sont nombreux �\n"
    "															avoir de vilains petits secrets...<br />SUSPENSE ET HUMOUR :\n"
    "															UNE COMEDIE POLICIERE IRRESISTIBLE !<br /><a\n"
    "																href=\"/auteur/Juliette-Sachs/498755\"\n"
    "																class=\"libelle\">Juliette Sachs</a>, dipl�m�e en droit,\n"
    "															est juriste en r�gion parisienne. <a\n"
    "																href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729\"\n"
    "																class=\"titre1\">Vacances mortelles au paradis</a> est son\n"
    "															deuxi�me roman, un cosy mystery � la fran�aise qui m�lange\n"
    "															habilement humour, amour et suspense.<br />J'ai d�vor� ce\n"
    "															roman que Slavka Miklusova, Attach�e de presse chez City\n"
    "															�ditons m'a permis de d�couvrir en service presse. Un livre,\n"
    "															qui contrairement � ce que l'on pourrait croire en voyant le\n"
    "															titre, fait du bien. Que d'�motions tout au long de cette\n"
    "															lecture et quel voyage dans cet endroit paradisiaque. Je ne\n"
    "															peux m'emp�cher de sourire quand je pense au comportement de\n"
    "															la maman d'Alice envers sa fille ainsi que son papa et sa\n"
    "															nouvelle jeune compagne. Un livre percutant et dr�le � la\n"
    "															fois ! Je ne vous en d�voile pas plus et je vous en\n"
    "															conseille fortement la lecture. Vous allez passer un\n"
    "															agr�able moment. N'oubliez pas votre maillot de bain et\n"
    "															votre tuba et j'en passe...\n"
    "\n"
    "															<br />\n"
    "															Lien : <a\n"
    "																href=\"http://binchy.canalblog.com/archives/2019/09/20/37649104.html\"\n"
    "																class=\"tiny_links\" rel=\"nofollow\"\n"
    "																target=\"blank\">http://binchy.canalblog.com/.. </a></div>\n"
    "														<a href=\"javascript:void(0);\"\n"
    "															onclick=\"javascript:humhum_voir_plus('cri_2017498');\"><span\n"
    "																class=\"tiny_links post_con_more text_hidden_display\">\n"
    "																<div class=\"text_hidden_display_open\"><span>+</span>\n"
    "																	Lire la suite</div>\n"
    "															</span></a>\n"
    "													</div>\n"
    "									</div>\n"
    "									</td>\n"
    "									</tr>\n"
    "									</table>\n"
    "									<div class=\"interaction post_items row\" style=\";\" id=\"par_2017498_1\"><a\n"
    "											href=\"javascript:void(0);\" class=\"post_items_links boutton_comm\"\n"
    "											id=\"btn_comm_2017498_1\" rel=\"nofollow\">Commenter</a><span> &nbsp</span><span\n"
    "											class=\"qualite\" name=\"\"><span id=\"myspan2017498\"><a\n"
    "													href=\"/aj_criU.php?id_item=2017498&id_user=-1&type=1\" rel=\"nofollow\"\n"
    "													onclick=\"javascript:get_app(2017498,-1,1,'par_2017498_1');return false;\"\n"
    "													class=\"post_items_links\">J�appr�cie�</a></span><span\n"
    "												style=\"width:50px;\"> &nbsp &nbsp &nbsp &nbsp </span><span\n"
    "												class=\"post_items_like \" style=\"margin-left: 5px;\"><span\n"
    "													class=\"icon-like\"></span><span id=\"myspanNB2017498\"\n"
    "													style=\"font-size: 0.58rem;\">6</span></span><a\n"
    "												href=\"javascript:void(0);\" class=\"post_items_com\"><span\n"
    "													class=\"icon-comment boutton_comm_icon\"\n"
    "													id=\"icon_comm_2017498_1\"></span><span id=\"myspanCM2017498\"\n"
    "													style=\"font-size: 0.58rem;\">0</span></a></span></div>\n"
    "									<div class=\"commentaires_container post_com\" id=\"comm_2017498_1\"></div>\n"
    "							</div>\n"
    "						</div>\n"
    "						<style media=\"screen\" type=\"text/css\">\n"
    "							.ratingblock {\n"
    "								float: right;\n"
    "								margin-right: 9px;\n"
    "							}\n"
    "						</style>\n"
    "						<div class=\"post\" itemscope itemtype=\"http://schema.org/Review\"><span itemprop=\"itemReviewed\"\n"
    "								itemscope itemtype=\"http://schema.org/Thing\">\n"
    "								<meta itemprop=\"name\" content=\"Vacances mortelles au paradis\" />\n"
    "								<meta itemprop=\"url\"\n"
    "content=\"http://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2018161\" />\n "
    "								<div class=\"post_con\" id=\"B_CRI2018161\">\n"
    "									<table>\n"
    "										<tr>\n"
    "											<td class=\"post_avatar not-mobile\"><a href=\"/monprofil.php?id_user=421761\"\n"
    "													rel=\"nofollow\" class=\"not-mobile\"><img src=\"/users/421761_a4999.jpg\"\n"
    "														alt=\"mariedigne\" class=\"not-mobile\" /></a></td>\n"
    "											<td>\n"
    "												<table style=\"width: 95%;\">\n"
    "													<tr>\n"
    "														<td class=\"no_img\"><a href=\"/monprofil.php?id_user=421761\"\n"
    "																class=\"author\" rel=\"nofollow\"><span itemprop=\"author\"\n"
    "																	itemscope itemtype=\"http://schema.org/Thing\"><span\n"
    "																		itemprop=\"name\">mariedigne</span>\n"
    "																	<meta itemprop=\"sameAs\"\n"
    "																		content=\"http://www.babelio.com/monprofil.php?id_user=421761\">\n"
    "																	</span></a> &nbsp;\n"
    "															<span class=\"gris\"\n"
    "																style='font-family: \"Open Sans\",sans-serif;'>21\n"
    "																septembre 2019</span><br></td>\n"
    "														<td style=\"text-align:right;\">\n"
    "															<div data-id=\"\" class=\"rateit\" data-rateit-mode=\"font\"\n"
    "																data-rateit-value=\"4.0\" data-rateit-ispreset=\"true\"\n"
    "																data-rateit-readonly=\"true\"\n"
    "																data-rateit-resetable=\"false\" data-rateit-icon=\"&#9733;\"\n"
    "																data-test-rateit=\"Else\"></div><span\n"
    "																itemprop=\"reviewRating\" itemscope\n"
    "																itemtype=\"http://schema.org/Rating\">\n"
    "																<meta itemprop=\"worstRating\" content=\"1\" />\n"
    "																<meta itemprop=\"ratingValue\" content=\"4.0\" />\n"
    "																<meta itemprop=\"bestRating\" content=\"5\" /></span>\n"
    "														</td>\n"
    "														<td>\n"
    "															<div class=\"dropdown\">\n"
    "																<div class=\"icon-blogger7\"></div>\n"
    "																<div class=\"dropdown_open\"><a\n"
    "																		href=\"/signaler_abus.php?id_item=2018161&type=1\"\n"
    "																		rel=\"nofollow\">Signaler ce contenu</a><a\n"
    "																		href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2018161\">Voir\n"
    "																		la page de la critique</a></div>\n"
    "															</div>\n"
    "														</td>\n"
    "													</tr>\n"
    "												</table>\n"
    "												<div class=\"text row\">\n"
    "													<div id=\"cri_2018161\" style=\"height:10.5em;overflow:hidden;\">\n"
    "\n"
    "														Un petit tour sur une �le priv�e des Maldives avec un \"Cozy\n"
    "														Mystery\" de <a href=\"/auteur/Juliette-Sachs/498755\"\n"
    "															class=\"libelle\">Juliette Sachs</a>, second roman publi� en\n"
    "														septembre. \"<a\n"
    "															href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729\"\n"
    "															class=\"titre1\">Vacances mortelles au paradis</a>\" est un\n"
    "														roman policier doubl� d'une com�die avec une dose de romance,\n"
    "														dont l'intrigue se d�roule au sein d'un groupe d'une centaine de\n"
    "														personnes invit�es � un mariage et qui ont eu la chance de se\n"
    "														voir offrir un s�jour d'une semaine dans une �le paradisiaque �\n"
    "														cette occasion. <br />\n"
    "														C'est la premi�re fois que je lis ce genre de roman et j'avoue\n"
    "														que j'ai pass� un tr�s bon moment ! Un roman court, certes\n"
    "														l�ger, mais dr�le et tr�s rafra�chissant dont le personnage\n"
    "														principal, Alice (soeur de la mari�e), entour�e d'un petit\n"
    "														groupe d'amis choisis, est exub�rante, fonceuse, pleine d'humour\n"
    "														et attachante. Les p�rip�ties d'Alice sont succulentes ; coinc�e\n"
    "														entre une m�re �touffante qui lui reproche de ne pas �tre mari�e\n"
    "														� 39 ans et qui est en qu�te du gendre id�al et un p�re remari�\n"
    "														avec une jeunette de 25 ans un peu loufoque... le style de ce\n"
    "														roman est particuli�rement moderne et bien rythm�. <br />\n"
    "														L'intrigue : une femme va �tre assassin�e au cours de la soir�e\n"
    "														du mariage, ce qui va obliger la police locale � bloquer tout\n"
    "														d�part de l'�le jusqu'� la r�solution de ce myst�rieux\n"
    "														assassinat. Devant la lenteur du lieutenant charg� de l'enqu�te,\n"
    "														Alice d�cide alors d'endosser le costume de d�tective et de se\n"
    "														lancer dans la d�couverte de l'assassin. C'est ce que l'on\n"
    "														appelle un \"Cozy Mystory\" qui est un sous-genre de la fiction\n"
    "														polici�re, beaucoup plus soft et souvent trait� avec humour\n"
    "														!<br />\n"
    "														Je ne vous en dis pas plus et vous laisse d�couvrir les\n"
    "														p�rip�ties qu'Alice va rencontrer ! A lire durant une journ�e\n"
    "														pluvieuse ou dans le train ;-)\n"
    "\n"
    "														<br />\n"
    "														Lien : <a\n"
    "href=\"https://mesailleurs.wordpress.com/2019/09/21/voyage-litteraire-de-la-semaine-vacances-mortelles-au"
    "-paradis-de-juliette-sachs/\"\n "
    "															class=\"tiny_links\" rel=\"nofollow\"\n"
    "															target=\"blank\">https://mesailleurs.wordpres.. </a></div><a\n"
    "														href=\"javascript:void(0);\"\n"
    "														onclick=\"javascript:humhum_voir_plus('cri_2018161');\"><span\n"
    "															class=\"tiny_links post_con_more text_hidden_display\">\n"
    "															<div class=\"text_hidden_display_open\"><span>+</span> Lire la\n"
    "																suite</div>\n"
    "														</span></a>\n"
    "												</div>\n"
    "								</div>\n"
    "								</td>\n"
    "								</tr>\n"
    "								</table>\n"
    "								<div class=\"interaction post_items row\" style=\";\" id=\"par_2018161_1\"><a\n"
    "										href=\"javascript:void(0);\" class=\"post_items_links boutton_comm\"\n"
    "										id=\"btn_comm_2018161_1\" rel=\"nofollow\">Commenter</a><span> &nbsp</span><span\n"
    "										class=\"qualite\" name=\"\"><span id=\"myspan2018161\"><a\n"
    "												href=\"/aj_criU.php?id_item=2018161&id_user=-1&type=1\" rel=\"nofollow\"\n"
    "												onclick=\"javascript:get_app(2018161,-1,1,'par_2018161_1');return false;\"\n"
    "												class=\"post_items_links\">J�appr�cie�</a></span><span\n"
    "											style=\"width:50px;\"> &nbsp &nbsp &nbsp &nbsp </span><span\n"
    "											class=\"post_items_like \" style=\"margin-left: 5px;\"><span\n"
    "												class=\"icon-like\"></span><span id=\"myspanNB2018161\"\n"
    "												style=\"font-size: 0.58rem;\">7</span></span><a href=\"javascript:void(0);\"\n"
    "											class=\"post_items_com\"><span class=\"icon-comment boutton_comm_icon\"\n"
    "												id=\"icon_comm_2018161_1\"></span><span id=\"myspanCM2018161\"\n"
    "												style=\"font-size: 0.58rem;\">0</span></a></span></div>\n"
    "								<div class=\"commentaires_container post_com\" id=\"comm_2018161_1\"></div>\n"
    "						</div>\n"
    "					</div>\n"
    "					<style media=\"screen\" type=\"text/css\">\n"
    "						.ratingblock {\n"
    "							float: right;\n"
    "							margin-right: 9px;\n"
    "						}\n"
    "					</style>\n"
    "					<div class=\"post\" itemscope itemtype=\"http://schema.org/Review\"><span itemprop=\"itemReviewed\"\n"
    "							itemscope itemtype=\"http://schema.org/Thing\">\n"
    "							<meta itemprop=\"name\" content=\"Vacances mortelles au paradis\" />\n"
    "							<meta itemprop=\"url\"\n"
    "content=\"http://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2019698\" />\n "
    "							<div class=\"post_con\" id=\"B_CRI2019698\">\n"
    "								<table>\n"
    "									<tr>\n"
    "										<td class=\"post_avatar not-mobile\"><a href=\"/monprofil.php?id_user=337539\"\n"
    "												rel=\"nofollow\" class=\"not-mobile\"><img src=\"/users/337539_a3013.jpeg\"\n"
    "													alt=\"bbpoussy\" class=\"not-mobile\" /></a></td>\n"
    "										<td>\n"
    "											<table style=\"width: 95%;\">\n"
    "												<tr>\n"
    "													<td class=\"no_img\"><a href=\"/monprofil.php?id_user=337539\"\n"
    "															class=\"author\" rel=\"nofollow\"><span itemprop=\"author\"\n"
    "																itemscope itemtype=\"http://schema.org/Thing\"><span\n"
    "																	itemprop=\"name\">bbpoussy</span>\n"
    "																<meta itemprop=\"sameAs\"\n"
    "																	content=\"http://www.babelio.com/monprofil.php?id_user=337539\">\n"
    "																</span></a> &nbsp;\n"
    "														<span class=\"gris\"\n"
    "															style='font-family: \"Open Sans\",sans-serif;'>23 septembre\n"
    "															2019</span><br></td>\n"
    "													<td style=\"text-align:right;\">\n"
    "														<div data-id=\"\" class=\"rateit\" data-rateit-mode=\"font\"\n"
    "															data-rateit-value=\"2.0\" data-rateit-ispreset=\"true\"\n"
    "															data-rateit-readonly=\"true\" data-rateit-resetable=\"false\"\n"
    "															data-rateit-icon=\"&#9733;\" data-test-rateit=\"Else\"></div>\n"
    "														<span itemprop=\"reviewRating\" itemscope\n"
    "															itemtype=\"http://schema.org/Rating\">\n"
    "															<meta itemprop=\"worstRating\" content=\"1\" />\n"
    "															<meta itemprop=\"ratingValue\" content=\"2.0\" />\n"
    "															<meta itemprop=\"bestRating\" content=\"5\" /></span>\n"
    "													</td>\n"
    "													<td>\n"
    "														<div class=\"dropdown\">\n"
    "															<div class=\"icon-blogger7\"></div>\n"
    "															<div class=\"dropdown_open\"><a\n"
    "																	href=\"/signaler_abus.php?id_item=2019698&type=1\"\n"
    "																	rel=\"nofollow\">Signaler ce contenu</a><a\n"
    "																	href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2019698\">Voir\n"
    "																	la page de la critique</a></div>\n"
    "														</div>\n"
    "													</td>\n"
    "												</tr>\n"
    "											</table>\n"
    "											<div class=\"text row\">\n"
    "												<div id=\"cri_2019698\">\n"
    "\n"
    "													Difficile de passer d'un tr�s bon polar de <a\n"
    "														href=\"/auteur/Michael-Connelly/2771\" class=\"libelle\">Micha�l\n"
    "														Connelly</a> � celui-ci o� l'h�ro�ne qui m�ne l'enqu�te le fait\n"
    "													entre un mojito et un plan drague. O� sa famille est assez loufoque.\n"
    "													La soeur se marie a une grosse famille bourgeoise, le p�re � la\n"
    "													bimbo de service et la m�re lui rabat les oreilles sur son\n"
    "													c�libat.<br />\n"
    "													Beaucoup de clich�s et une fin que l'on voit venir d�s le\n"
    "													d�but.<br />\n"
    "													L'avantage c'est qu'avec seulement 272 pages, il se lit tr�s vite.\n"
    "\n"
    "\n"
    "												</div>\n"
    "											</div>\n"
    "							</div>\n"
    "							</td>\n"
    "							</tr>\n"
    "							</table>\n"
    "							<div class=\"interaction post_items row\" style=\";\" id=\"par_2019698_1\"><a\n"
    "									href=\"javascript:void(0);\" class=\"post_items_links boutton_comm\"\n"
    "									id=\"btn_comm_2019698_1\" rel=\"nofollow\">Commenter</a><span> &nbsp</span><span\n"
    "									class=\"qualite\" name=\"\"><span id=\"myspan2019698\"><a\n"
    "											href=\"/aj_criU.php?id_item=2019698&id_user=-1&type=1\" rel=\"nofollow\"\n"
    "											onclick=\"javascript:get_app(2019698,-1,1,'par_2019698_1');return false;\"\n"
    "											class=\"post_items_links\">J�appr�cie�</a></span><span style=\"width:50px;\">\n"
    "										&nbsp &nbsp &nbsp &nbsp </span><span class=\"post_items_like \"\n"
    "										style=\"margin-left: 5px;\"><span class=\"icon-like\"></span><span\n"
    "											id=\"myspanNB2019698\" style=\"font-size: 0.58rem;\">8</span></span><a\n"
    "										href=\"javascript:void(0);\" class=\"post_items_com\"><span\n"
    "											class=\"icon-comment boutton_comm_icon\" id=\"icon_comm_2019698_1\"></span><span\n"
    "											id=\"myspanCM2019698\" style=\"font-size: 0.58rem;\">0</span></a></span></div>\n"
    "							<div class=\"commentaires_container post_com\" id=\"comm_2019698_1\"></div>\n"
    "					</div>\n"
    "				</div>\n"
    "				<style media=\"screen\" type=\"text/css\">\n"
    "					.ratingblock {\n"
    "						float: right;\n"
    "						margin-right: 9px;\n"
    "					}\n"
    "				</style>\n"
    "<div class=\"post\" itemscope itemtype=\"http://schema.org/Review\"><span itemprop=\"itemReviewed\" itemscope\n "
    "						itemtype=\"http://schema.org/Thing\">\n"
    "						<meta itemprop=\"name\" content=\"Vacances mortelles au paradis\" />\n"
    "						<meta itemprop=\"url\"\n"
    "content=\"http://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2014865\" />\n "
    "						<div class=\"post_con\" id=\"B_CRI2014865\">\n"
    "							<table>\n"
    "								<tr>\n"
    "									<td class=\"post_avatar not-mobile\"><a href=\"/monprofil.php?id_user=461499\"\n"
    "											rel=\"nofollow\" class=\"not-mobile\"><img src=\"/users/461499_a7916.jpg\"\n"
    "												alt=\"malaufg\" class=\"not-mobile\" /></a></td>\n"
    "									<td>\n"
    "										<table style=\"width: 95%;\">\n"
    "											<tr>\n"
    "												<td class=\"no_img\"><a href=\"/monprofil.php?id_user=461499\"\n"
    "														class=\"author\" rel=\"nofollow\"><span itemprop=\"author\" itemscope\n"
    "															itemtype=\"http://schema.org/Thing\"><span\n"
    "																itemprop=\"name\">malaufg</span>\n"
    "															<meta itemprop=\"sameAs\"\n"
    "																content=\"http://www.babelio.com/monprofil.php?id_user=461499\">\n"
    "															</span></a> &nbsp;\n"
    "													<span class=\"gris\" style='font-family: \"Open Sans\",sans-serif;'>17\n"
    "														septembre 2019</span><br></td>\n"
    "												<td style=\"text-align:right;\">\n"
    "													<div data-id=\"\" class=\"rateit\" data-rateit-mode=\"font\"\n"
    "														data-rateit-value=\"5.0\" data-rateit-ispreset=\"true\"\n"
    "														data-rateit-readonly=\"true\" data-rateit-resetable=\"false\"\n"
    "														data-rateit-icon=\"&#9733;\" data-test-rateit=\"Else\"></div><span\n"
    "														itemprop=\"reviewRating\" itemscope\n"
    "														itemtype=\"http://schema.org/Rating\">\n"
    "														<meta itemprop=\"worstRating\" content=\"1\" />\n"
    "														<meta itemprop=\"ratingValue\" content=\"5.0\" />\n"
    "														<meta itemprop=\"bestRating\" content=\"5\" /></span>\n"
    "												</td>\n"
    "												<td>\n"
    "													<div class=\"dropdown\">\n"
    "														<div class=\"icon-blogger7\"></div>\n"
    "														<div class=\"dropdown_open\"><a\n"
    "																href=\"/signaler_abus.php?id_item=2014865&type=1\"\n"
    "																rel=\"nofollow\">Signaler ce contenu</a><a\n"
    "																href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2014865\">Voir\n"
    "																la page de la critique</a></div>\n"
    "													</div>\n"
    "												</td>\n"
    "											</tr>\n"
    "										</table>\n"
    "										<div class=\"text row\">\n"
    "											<div id=\"cri_2014865\">\n"
    "\n"
    "												J'ai ador�!!! ce n'est pas difficile, je l'ai commenc� en d�but\n"
    "												d'apr�s-midi et d�vor� d'une traite! Ce livre est bourr� d'humour : les\n"
    "												parents d'Alice sont in�narrables! <br />Cocasse, distrayant, et\n"
    "												p�tillant, plein de rebondissements, le tout men� tambour battant par\n"
    "												une Alice absolument d�chain�e et d�jant�e.<br /> En m�me temps, c'est\n"
    "												un polar plut�t bien ficel� avec une pointe de romance. Je recommande\n"
    "												compl�tement car j'ai vraiment pass� un excellent moment.\n"
    "\n"
    "\n"
    "											</div>\n"
    "										</div>\n"
    "						</div>\n"
    "						</td>\n"
    "						</tr>\n"
    "						</table>\n"
    "						<div class=\"interaction post_items row\" style=\";\" id=\"par_2014865_1\"><a\n"
    "								href=\"javascript:void(0);\" class=\"post_items_links boutton_comm\" id=\"btn_comm_2014865_1\"\n"
    "								rel=\"nofollow\">Commenter</a><span> &nbsp</span><span class=\"qualite\" name=\"\"><span\n"
    "									id=\"myspan2014865\"><a href=\"/aj_criU.php?id_item=2014865&id_user=-1&type=1\"\n"
    "										rel=\"nofollow\"\n"
    "										onclick=\"javascript:get_app(2014865,-1,1,'par_2014865_1');return false;\"\n"
    "										class=\"post_items_links\">J�appr�cie�</a></span><span style=\"width:50px;\"> &nbsp\n"
    "									&nbsp &nbsp &nbsp </span><span class=\"post_items_like \"\n"
    "									style=\"margin-left: 5px;\"><span class=\"icon-like\"></span><span id=\"myspanNB2014865\"\n"
    "										style=\"font-size: 0.58rem;\">6</span></span><a href=\"javascript:void(0);\"\n"
    "									class=\"post_items_com\"><span class=\"icon-comment boutton_comm_icon\"\n"
    "										id=\"icon_comm_2014865_1\"></span><span id=\"myspanCM2014865\"\n"
    "										style=\"font-size: 0.58rem;\">0</span></a></span></div>\n"
    "						<div class=\"commentaires_container post_com\" id=\"comm_2014865_1\"></div>\n"
    "				</div>\n"
    "			</div>\n"
    "			<style media=\"screen\" type=\"text/css\">\n"
    "				.ratingblock {\n"
    "					float: right;\n"
    "					margin-right: 9px;\n"
    "				}\n"
    "			</style>\n"
    "			<div class=\"post\" itemscope itemtype=\"http://schema.org/Review\"><span itemprop=\"itemReviewed\" itemscope\n"
    "					itemtype=\"http://schema.org/Thing\">\n"
    "					<meta itemprop=\"name\" content=\"Vacances mortelles au paradis\" />\n"
    "					<meta itemprop=\"url\"\n"
    "						content=\"http://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2016409\" />\n"
    "					<div class=\"post_con\" id=\"B_CRI2016409\">\n"
    "						<table>\n"
    "							<tr>\n"
    "								<td class=\"post_avatar not-mobile\"><a href=\"/monprofil.php?id_user=731680\"\n"
    "										rel=\"nofollow\" class=\"not-mobile\"><img src=\"/users/731680_a4540.jpg\"\n"
    "											alt=\"daphnee77\" class=\"not-mobile\" /></a></td>\n"
    "								<td>\n"
    "									<table style=\"width: 95%;\">\n"
    "										<tr>\n"
    "											<td class=\"no_img\"><a href=\"/monprofil.php?id_user=731680\" class=\"author\"\n"
    "													rel=\"nofollow\"><span itemprop=\"author\" itemscope\n"
    "														itemtype=\"http://schema.org/Thing\"><span\n"
    "															itemprop=\"name\">daphnee77</span>\n"
    "														<meta itemprop=\"sameAs\"\n"
    "															content=\"http://www.babelio.com/monprofil.php?id_user=731680\">\n"
    "														</span></a> &nbsp;\n"
    "												<span class=\"gris\" style='font-family: \"Open Sans\",sans-serif;'>19\n"
    "													septembre 2019</span><br></td>\n"
    "											<td style=\"text-align:right;\">\n"
    "												<div data-id=\"\" class=\"rateit\" data-rateit-mode=\"font\"\n"
    "													data-rateit-value=\"5.0\" data-rateit-ispreset=\"true\"\n"
    "													data-rateit-readonly=\"true\" data-rateit-resetable=\"false\"\n"
    "													data-rateit-icon=\"&#9733;\" data-test-rateit=\"Else\"></div><span\n"
    "													itemprop=\"reviewRating\" itemscope\n"
    "													itemtype=\"http://schema.org/Rating\">\n"
    "													<meta itemprop=\"worstRating\" content=\"1\" />\n"
    "													<meta itemprop=\"ratingValue\" content=\"5.0\" />\n"
    "													<meta itemprop=\"bestRating\" content=\"5\" /></span>\n"
    "											</td>\n"
    "											<td>\n"
    "												<div class=\"dropdown\">\n"
    "													<div class=\"icon-blogger7\"></div>\n"
    "													<div class=\"dropdown_open\"><a\n"
    "															href=\"/signaler_abus.php?id_item=2016409&type=1\"\n"
    "															rel=\"nofollow\">Signaler ce contenu</a><a\n"
    "															href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/critiques/2016409\">Voir\n"
    "															la page de la critique</a></div>\n"
    "												</div>\n"
    "											</td>\n"
    "										</tr>\n"
    "									</table>\n"
    "									<div class=\"text row\">\n"
    "										<div id=\"cri_2016409\">\n"
    "\n"
    "											Belle com�die polici�re sur fond de romance qui se lit tr�s vite tant l'on a\n"
    "											envie de suivre les aventures d'Alice, enqu�trice tr�s attachante. <br />\n"
    "											Dr�le, frais, agr�able et bien �crit ce qui ne g�che rien. A consommer sans\n"
    "											mod�ration !\n"
    "\n"
    "\n"
    "										</div>\n"
    "									</div>\n"
    "					</div>\n"
    "					</td>\n"
    "					</tr>\n"
    "					</table>\n"
    "					<div class=\"interaction post_items row\" style=\";\" id=\"par_2016409_1\"><a href=\"javascript:void(0);\"\n"
    "							class=\"post_items_links boutton_comm\" id=\"btn_comm_2016409_1\"\n"
    "							rel=\"nofollow\">Commenter</a><span> &nbsp</span><span class=\"qualite\" name=\"\"><span\n"
    "								id=\"myspan2016409\"><a href=\"/aj_criU.php?id_item=2016409&id_user=-1&type=1\"\n"
    "									rel=\"nofollow\"\n"
    "									onclick=\"javascript:get_app(2016409,-1,1,'par_2016409_1');return false;\"\n"
    "									class=\"post_items_links\">J�appr�cie�</a></span><span style=\"width:50px;\"> &nbsp\n"
    "								&nbsp &nbsp &nbsp </span><span class=\"post_items_like \" style=\"margin-left: 5px;\"><span\n"
    "									class=\"icon-like\"></span><span id=\"myspanNB2016409\"\n"
    "									style=\"font-size: 0.58rem;\">6</span></span><a href=\"javascript:void(0);\"\n"
    "								class=\"post_items_com\"><span class=\"icon-comment boutton_comm_icon\"\n"
    "									id=\"icon_comm_2016409_1\"></span><span id=\"myspanCM2016409\"\n"
    "									style=\"font-size: 0.58rem;\">0</span></a></span></div>\n"
    "					<div class=\"commentaires_container post_com\" id=\"comm_2016409_1\"></div>\n"
    "			</div>\n"
    "		</div>\n"
    "	</div>\n"
    "	<div class=\"titre\" id=\"citations\">\n"
    "		Citations et extraits (2)\n"
    "\n"
    "		<a href=\"/connection.php\" rel=\"nofollow\" class=\"tiny_links only-desktop\">\n"
    "\n"
    "			Ajouter une citation</a></div>\n"
    "	<div class=\"side_l_content\">\n"
    "		<div class=\"post\">\n"
    "			<div class=\"post_con\">\n"
    "				<table>\n"
    "					<tr>\n"
    "						<td class=\"post_avatar not-mobile\"><a href=\"/monprofil.php?id_user=337539\" rel=\"nofollow\"><img\n"
    "									src=\"/users/337539_a3013.jpeg\" alt=\"bbpoussy\" /></a></td>\n"
    "						<td><a href=\"/monprofil.php?id_user=337539\" class=\"author\" rel=\"nofollow\">bbpoussy</a> &nbsp;\n"
    "							<span class=\"gris\">22 septembre 2019</span>\n"
    "							<div class=\"dropdown\">\n"
    "								<div class=\"icon-blogger7\"></div>\n"
    "								<div class=\"dropdown_open\"><a href=\"/signaler_abus.php?id_item=1827052&type=2\">Signaler\n"
    "										ce contenu</a><a href=\"/auteur/Juliette-Sachs/498755/citations/1827052\">Voir la\n"
    "										page de la citation</a></div>\n"
    "							</div>\n"
    "							<div class=\"text row\">\n"
    "								<div id=\"cit0\">\n"
    "									A quoi cela sert-il que je passe deux heures par semaine � la salle de sport pour\n"
    "									entretenir ma silhouette alors que la prochaine personne qui verra mon corps nu sera\n"
    "									le m�decin l�giste lorsque j'aurai �t� bouff�e par mes chats ?\n"
    "\n"
    "								</div>\n"
    "							</div>\n"
    "						</td>\n"
    "					</tr>\n"
    "				</table>\n"
    "				<div class=\"interaction post_items row\" style=\";\" id=\"par_1827052_2\"><a href=\"javascript:void(0);\"\n"
    "						class=\"post_items_links boutton_comm\" id=\"btn_comm_1827052_2\" rel=\"nofollow\">Commenter</a><span>\n"
    "						&nbsp</span><span class=\"qualite\" name=\"\"><span id=\"myspan1827052\"><a\n"
    "								href=\"/aj_criU.php?id_item=1827052&id_user=-1&type=2\" rel=\"nofollow\"\n"
    "								onclick=\"javascript:get_app(1827052,-1,2,'par_1827052_2');return false;\"\n"
    "								class=\"post_items_links\">J�appr�cie�</a></span><span style=\"width:50px;\"> &nbsp &nbsp\n"
    "							&nbsp &nbsp </span><span class=\"post_items_like \" style=\"margin-left: 5px;\"><span\n"
    "								class=\"icon-like\"></span><span id=\"myspanNB1827052\"\n"
    "								style=\"font-size: 0.58rem;\">5</span></span><a href=\"javascript:void(0);\"\n"
    "							class=\"post_items_com\"><span class=\"icon-comment boutton_comm_icon\"\n"
    "								id=\"icon_comm_1827052_2\"></span><span id=\"myspanCM1827052\"\n"
    "								style=\"font-size: 0.58rem;\">0</span></a></span></div>\n"
    "				<div class=\"commentaires_container post_com\" id=\"comm_1827052_2\"></div>\n"
    "			</div>\n"
    "		</div>\n"
    "		<div class=\"post\">\n"
    "			<div class=\"post_con\">\n"
    "				<table>\n"
    "					<tr>\n"
    "						<td class=\"post_avatar not-mobile\"><a href=\"/monprofil.php?id_user=18400\" rel=\"nofollow\"><img\n"
    "									src=\"/\" alt=\"missmolko1\" /></a></td>\n"
    "						<td><a href=\"/monprofil.php?id_user=18400\" class=\"author\" rel=\"nofollow\">missmolko1</a> &nbsp;\n"
    "							<span class=\"gris\">08 septembre 2019</span>\n"
    "							<div class=\"dropdown\">\n"
    "								<div class=\"icon-blogger7\"></div>\n"
    "								<div class=\"dropdown_open\"><a href=\"/signaler_abus.php?id_item=1817708&type=2\">Signaler\n"
    "										ce contenu</a><a href=\"/auteur/Juliette-Sachs/498755/citations/1817708\">Voir la\n"
    "										page de la citation</a></div>\n"
    "							</div>\n"
    "							<div class=\"text row\">\n"
    "								<div id=\"cit1\" style=\"height:12em;overflow:hidden;\">\n"
    "									Les pieds dans l�eau transparente, j�observe de minuscules b�b�s requins nager\n"
    "									autour de mes chevilles. C�est difficile d�imaginer que ces inoffensives cr�atures\n"
    "									d�� peine vingt centim�tres se transformeront bient�t en de redoutables pr�dateurs.\n"
    "									Je fais quelques pas au bord de l�eau et prends le temps d�admirer le sable d�un\n"
    "									blanc immacul�, sur lequel s��parpillent des cocotiers. Je ferme les yeux pour\n"
    "									sentir sur ma peau la caresse du soleil. Cet endroit est paradisiaque.<br />\n"
    "									� Alice ! Tiens-toi droite et rentre le ventre, on dirait que tu es enceinte !<br />\n"
    "									Enfin, paradisiaque si l�on excepte ma m�re.<br />\n"
    "									� Fous-lui la paix Mireille ! �a ne te suffit pas de m�avoir pourri la vie pendant\n"
    "									trente ans ? Il faut aussi que tu tortures mes enfants ?<br />\n"
    "									Et mon p�re.<br />\n"
    "									� Mon lapin, ne t��nerve pas, c�est mauvais pour ta tension !<br />\n"
    "									Et sa nouvelle femme de vingt-cinq ans �pous�e un mois plus t�t.<br />\n"
    "									� Toi la p�tasse, on ne t�a pas sonn�e ! hurle ma m�re � pleins poumons.<br />\n"
    "									� Dis donc, tu parles � Cindy sur un autre ton ! rugit mon p�re en bondissant de son\n"
    "									transat.<br />\n"
    "									� Maman, papa, calmez-vous tous les deux. On est une famille et je suis s�re que\n"
    "									vous ne voudriez pas g�cher mon mariage.<br />\n"
    "									Et ma s�ur Miss Perfection, qui va bient�t �pouser monsieur Perfection. Non contente\n"
    "									d�avoir suivi sagement les �tudes brillantes que mes parents avaient toujours\n"
    "									souhait�es pour nous et d�avoir un travail stable et bien pay� de responsable\n"
    "									juridique au sein d�une grande banque, elle s�appr�te � �pouser, � tout juste trente\n"
    "									ans, le mari et gendre id�al, � savoir un mec beau-riche-gentil-intelligent-sportif.\n"
    "									Le type d�homme sur lequel toutes les femmes fantasment depuis leur plus jeune �ge\n"
    "									et dont tous les parents r�vent pour leur prog�niture ch�rie.\n"
    "\n"
    "								</div><a href=\"javascript:void(0);\" onclick=\"javascript:humhum('cit1');\"><span\n"
    "										class=\"tiny_links post_con_more text_hidden_display\">\n"
    "										<div class=\"text_hidden_display_open\"><span>+</span> Lire la suite</div>\n"
    "									</span></a>\n"
    "							</div>\n"
    "						</td>\n"
    "					</tr>\n"
    "				</table>\n"
    "				<div class=\"interaction post_items row\" style=\";\" id=\"par_1817708_2\"><a href=\"javascript:void(0);\"\n"
    "						class=\"post_items_links boutton_comm\" id=\"btn_comm_1817708_2\" rel=\"nofollow\">Commenter</a><span>\n"
    "						&nbsp</span><span class=\"qualite\" name=\"\"><span id=\"myspan1817708\"><a\n"
    "								href=\"/aj_criU.php?id_item=1817708&id_user=-1&type=2\" rel=\"nofollow\"\n"
    "								onclick=\"javascript:get_app(1817708,-1,2,'par_1817708_2');return false;\"\n"
    "								class=\"post_items_links\">J�appr�cie�</a></span><span style=\"width:50px;\"> &nbsp &nbsp\n"
    "							&nbsp &nbsp </span><span class=\"post_items_like \" style=\"margin-left: 5px;\"><span\n"
    "								class=\"icon-like\"></span><span id=\"myspanNB1817708\"\n"
    "								style=\"font-size: 0.58rem;\">2</span></span><a href=\"javascript:void(0);\"\n"
    "							class=\"post_items_com\"><span class=\"icon-comment boutton_comm_icon\"\n"
    "								id=\"icon_comm_1817708_2\"></span><span id=\"myspanCM1817708\"\n"
    "								style=\"font-size: 0.58rem;\">0</span></a></span></div>\n"
    "				<div class=\"commentaires_container post_com\" id=\"comm_1817708_2\"></div>\n"
    "			</div>\n"
    "		</div>\n"
    "	</div>\n"
    "	<div class=\"titre\">\n"
    "autres livres class�s : <a href=\"/livres-/Maldives/88625\" class=\"\">Maldives</a><a "
    "href=\"/livres-/Maldives/88625\"\n "
    "			class=\"more\"><span></span>Voir plus</a></div>\n"
    "	<div class=\"list_livre_con\">\n"
    "<div class=\"col col-2-4 list_livre\" style=\"max-width:120px;\" itemscope "
    "itemtype=\"http://schema.org/Book\"><a\n "
    "				href=\"/livres/Hees-Ces-psychopathes-qui-nous-gouvernent/1070690\"><img loading=\"lazy\"\n"
    "					src=\"https://images-eu.ssl-images-amazon.com/images/I/41x3yE9gFDL._SX95_.jpg\"\n"
    "					alt=\"Ces psychopathes qui nous gouvernent par Hees\"\n"
    "					title=\"Ces psychopathes qui nous gouvernent par Hees\"\n"
    "					onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "				<h2 itemprop=\"name\" style=\"\">Ces psychopathes qui nous gouvernent</h2>\n"
    "			</a><a href=\"/auteur/Jean-Luc-Hees/207521\">\n"
    "				<h3 style=\"margin:0px\">Jean-Luc Hees</h3>\n"
    "			</a>\n"
    "			<h3 style=\"margin:5px 0 0 0\">\n"
    "				<nobr><a href=\"/livres/Hees-Ces-psychopathes-qui-nous-gouvernent/1070690#critiques\"\n"
    "						class=\"titre_livre_elements\" style=\"color: #7F7F7F;\"><strong>4</strong>\n"
    "						critiques </a></nobr> &nbsp;\n"
    "			</h3>\n"
    "		</div>\n"
    "<div class=\"col col-2-4 list_livre\" style=\"max-width:120px;\" itemscope "
    "itemtype=\"http://schema.org/Book\"><a\n "
    "				href=\"/livres/Panthou-Guides-bleus-evasion-Sri-Lanka-et-Maldives/37946\"><img loading=\"lazy\"\n"
    "					src=\"https://images-na.ssl-images-amazon.com/images/I/51JA8NCHBXL._SX95_.jpg\"\n"
    "					alt=\"Guides bleus �vasion. Sri Lanka et Maldives par Panthou\"\n"
    "					title=\"Guides bleus �vasion. Sri Lanka et Maldives par Panthou\"\n"
    "					onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "				<h2 itemprop=\"name\" style=\"\">Guides bleus �vasion. Sri Lanka et Maldives</h2>\n"
    "			</a><a href=\"/auteur/Patrick-de-Panthou/187746\">\n"
    "				<h3 style=\"margin:0px\">Patrick de Panthou</h3>\n"
    "			</a>\n"
    "			<h3 style=\"margin:5px 0 0 0\"></h3>\n"
    "		</div>\n"
    "<div class=\"col col-2-4 list_livre\" style=\"max-width:120px;\" itemscope "
    "itemtype=\"http://schema.org/Book\"><a\n "
    "				href=\"/livres/Ghisotti-Les-poissons-des-Maldives/24822\"><img loading=\"lazy\"\n"
    "					src=\"/images/couv-defaut-grande.jpg\" alt=\"Les poissons des Maldives par Ghisotti\"\n"
    "					title=\"Les poissons des Maldives par Ghisotti\"\n"
    "					onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "				<h2 itemprop=\"name\" style=\"\">Les poissons des Maldives</h2>\n"
    "			</a><a href=\"/auteur/Andrea-Ghisotti/161446\">\n"
    "				<h3 style=\"margin:0px\">Andrea Ghisotti</h3>\n"
    "			</a>\n"
    "			<h3 style=\"margin:5px 0 0 0\"></h3>\n"
    "		</div>\n"
    "	</div>\n"
    "	</div>\n"
    "	<div class=\"side_r\">\n"
    "		<style media=\"screen\" type=\"text/css\">\n"
    "			.piclib:hover {\n"
    "				opacity: 0.6;\n"
    "				filter: alpha(opacity=60);\n"
    "				/* For IE8 and earlier */\n"
    "			}\n"
    "		</style>\n"
    "		<div class=\"side_r_content\">\n"
    "			<div class=\"titre\">\n"
    "				Acheter ce livre sur\n"
    "			</div><br>\n"
    "			<table>\n"
    "				<tr>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=e4474f47b8229372545311f36b239b7aHR0cDovL2V1bHRlY2guZm5hYy5jb20vZHluY2xpY2svZm5hYy8"
    "/ZXB0LXB1Ymxpc2hlcj1CYWJlbGlvJmVwdC1uYW1lPWdlbmVyaXF1ZSZldXJsPWh0dHA6Ly9yZWNoZXJjaGUuZm5hYy5jb20vU2VhcmNoUmVzdWx0L"
    "1Jlc3VsdExpc3QuYXNweD9TQ2F0PTIlMjExJlNlYXJjaD05NzgyODI0NjE1NDE3JnNmdD0xJnNhPTAmT3JpZ2luPVBBX0JBQkVMSU8=\"\n "
    "							target=\"_blank\" rel=\"nofollow\"><img src=\"/users/fnac2.jpg\" title=\"Fnac\" alt=\"Fnac\"\n"
    "								class=\"piclib\" style=\"width:62px;\" /></a></td>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=06ff2ecf809873925c52fc60b76a986aHR0cDovL3d3dy5hbWF6b24uZnIvZ3AvcHJvZHVjdC8yODI0NjE1NDE5P2llPVVURjgmdGFnPWJhYmVsaW"
    "8tMjEmbGlua0NvZGU9YXMyJmNhbXA9MTY0MiZjcmVhdGl2ZT02NzQ2JmNyZWF0aXZlQVNJTj0yODI0NjE1NDE5\"\n "
    "							target=\"_blank\" rel=\"nofollow\"><img src=\"/users/amazon2.jpg\" title=\"Amazon\" alt=\"Amazon\"\n"
    "								class=\"piclib\" style=\"width:62px;\" /></a></td>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=c95ae6f631c243fa4040358626dde60aHR0cDovL3RyYWNrLmVmZmlsaWF0aW9uLmNvbS9zZXJ2bGV0L2VmZmkucmVkaXI"
    "/aWRfY29tcHRldXI9MTM0MTE2MjQmdXJsPWh0dHA6Ly93d3cucHJpY2VtaW5pc3Rlci5jb20vcy85NzgyODI0NjE1NDE3\"\n "
    "							target=\"_blank\" rel=\"nofollow\"><img src=\"/users/logorakuten2.png\" title=\"Rakuten\"\n"
    "								alt=\"Rakuten\" class=\"piclib\" style=\"width:62px;\" /></a></td>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=55ebe2a3cc55fce3d05cb88d6bd35c1aHR0cHM6Ly93d3cuY3VsdHVyYS5jb20vY2F0YWxvZ3NlYXJjaC9yZXN1bHQvP3E9OTc4MjgyNDYxNTQxNz"
    "91dG1fc291cmNlPWJhYmVsaW8lMjZ1dG1fbWVkaXVtPWFmZmlsaWF0aW9uJTI2dXRtX2NhbXBhaWduPWFmZmluaXRhaXJlI2FlODQ2\"\n "
    "							target=\"_blank\" rel=\"nofollow\"><img src=\"/users/aff-cultura.jpg\" title=\"Cultura\"\n"
    "								alt=\"Cultura\" class=\"piclib\" style=\"width:62px;\" /></a></td>\n"
    "<td><a href=\"https://www.babelio.com/preprat.php?bBb"
    "=e5340d415f4218c4d35309cba8b104caHR0cHM6Ly93d3cuYXdpbjEuY29tL2NyZWFkLnBocD9hd2lubWlkPTc0ODEmYXdpbmFmZmlkPTQ5MTAyOS"
    "ZjbGlja3JlZj0mcD1odHRwcyUzQSUyRiUyRnQubmVvcnktdG0ubmV0JTJGdG0lMkZhJTJGY2hhbm5lbCUyRnRyYWNrZXIlMkZlYTJjYjE0ZTQ4JTNG"
    "dG1yZGUlM0RodHRwcyUzQSUyRiUyRnd3dy5tb21veC1zaG9wLmZyJTJGcHJvZHVpdHMtQzAlMkYlM0ZzZWFyY2hwYXJhbSUzRDk3ODI4MjQ2MTU0MT"
    "c=\"\n "
    "							target=\"_blank\" rel=\"nofollow\"><img src=\"/users/momox2.jpg\" title=\"Momox\" alt=\"Momox\"\n"
    "								class=\"piclib\" style=\"width:62px;\" /></a></td>\n"
    "				</tr>\n"
    "			</table>\n"
    "		</div>\n"
    "		<div class=\"side_share row\"><a href=\"/connection.php\" class=\"icon-blogger6 share_mail svg-icon\"></a>\n"
    "			<script type=\"text/javascript\">\n"
    "				function fbs_click() {\n"
    "u = location.href; t = document.title; window.open('https://www.facebook.com/sharer.php?u=' + "
    "encodeURIComponent(u) + '&amp;t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,"
    "height=436');\n "
    "					return false;\n"
    "				}\n"
    "</script><a href=\"https://www.facebook.com/share.php?u=https://www.babelio.com/livres/Sachs-Vacances-mortelles"
    "-au-paradis/1166729\"\n "
    "				onclick=\"return fbs_click()\" target=\"_blank\" class=\"icon-blogger4 share_fb svg-icon\"></a><a\n"
    "href=\"https://www.pinterest.com/pin/create/button/?url=https://www.babelio.com/livres/Sachs-Vacances-mortelles"
    "-au-paradis/1166729&media=https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX195_.jpg&description"
    "=Vacances mortelles au paradis - Juliette Sachs - Babelio\"\n "
    "				target=\"_blank\" rel=\"nofollow\" class=\"icon-blogger3 share_pin svg-icon\"></a><a\n"
    "href=\"https://twitter.com/intent/tweet?source=tweetbutton&text=Vacances mortelles au paradis - Juliette Sachs - "
    "Babelio&url=https://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis/1166729\"\n "
    "				class=\"icon-blogger5 share_tw svg-icon\" rel=\"nofollow\" target=\"_blank\"></a><a\n"
    "href=\"https://plus.google.com/share?url=https://www.babelio.com/livres/Sachs-Vacances-mortelles-au-paradis"
    "/1166729\"\n "
    "				class=\"icon-blogger2 share_gg svg-icon\" rel=\"nofollow\" target=\"_blank\"></a><a\n"
    "				href=\"/livres/Sachs-Vacances-mortelles-au-paradis/1166729/bloguer\" rel=\"nofollow\"\n"
    "				class=\"share_int\">Int�grer</a></div>\n"
    "		<div class=\"sep\"></div><br>\n"
    "		<div id='div-gpt-ad-1442725480364-2' style=\"text-align:center;\">\n"
    "			<script type='text/javascript'>\n"
    "				googletag.cmd.push(function () { googletag.display('div-gpt-ad-1442725480364-2'); });\n"
    "			</script>\n"
    "		</div>\n"
    "		<div class=\"side_r_content\">\n"
    "			<div class=\"titre\">\n"
    "				Vous aimez ce livre ? Babelio vous sugg�re\n"
    "\n"
    "			</div>\n"
    "			<div class=\"list_livre_con row\">\n"
    "<div class=\"col col-4 list_livre\" style=\"max-width:120px;\" itemscope itemtype=\"http://schema.org/Book\">\n "
    "					<a href=\"/livres/Chapman-Les-detectives-du-Yorkshire-tome-2--Rendez-vous-a/1037448\"><img\n"
    "							loading=\"lazy\" src=\"/couv/cvt_Les-Detectives-du-Yorkshire-Tome-2-Rendez-Vous-a_2029.jpg\"\n"
    "							alt=\"Les d�tectives du Yorkshire, tome 2 : Rendez-vous avec le mal par Chapman\"\n"
    "							title=\"Les d�tectives du Yorkshire, tome 2 : Rendez-vous avec le mal par Chapman\"\n"
    "							onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "						<h2 itemprop=\"name\" style=\"	overflow:hidden; padding-bottom:2px;padding-top:2px;\">Les d�tectives\n"
    "							du Yorkshire, t..</h2>\n"
    "					</a><a href=\"/auteur/Julia-Chapman/465165\">\n"
    "						<h3 style=\"margin:0px\">Julia Chapman</h3>\n"
    "					</a>\n"
    "					<h3 style=\"margin:5px 0 0 0\"></h3>\n"
    "				</div>\n"
    "<div class=\"col col-4 list_livre\" style=\"max-width:120px;\" itemscope itemtype=\"http://schema.org/Book\">\n "
    "					<a href=\"/livres/Beaton-Hamish-Macbeth-tome-2--Qui-va-a-la-chasse/1140111\"><img loading=\"lazy\"\n"
    "							src=\"https://images-eu.ssl-images-amazon.com/images/I/51ujAYArMiL._SX95_.jpg\"\n"
    "							alt=\"Hamish Macbeth, tome 2 : Qui va � la chasse par Beaton\"\n"
    "							title=\"Hamish Macbeth, tome 2 : Qui va � la chasse par Beaton\"\n"
    "							onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "						<h2 itemprop=\"name\" style=\"	overflow:hidden; padding-bottom:2px;padding-top:2px;\">Hamish\n"
    "							Macbeth, tome 2 : Qui..</h2>\n"
    "					</a><a href=\"/auteur/MC-Beaton/262506\">\n"
    "						<h3 style=\"margin:0px\">M.C. Beaton</h3>\n"
    "					</a>\n"
    "					<h3 style=\"margin:5px 0 0 0\"></h3>\n"
    "				</div>\n"
    "<div class=\"col col-4 list_livre\" style=\"max-width:120px;\" itemscope itemtype=\"http://schema.org/Book\">\n "
    "					<a href=\"/livres/Evanovich-Stephanie-Plum-tome-18--Ya-pas-18-solutions/982293\"><img loading=\"lazy\"\n"
    "							src=\"/couv/cvt_Stephanie-Plum-tome-18--Ya-pas-18-solutions_1285.jpg\"\n"
    "							alt=\"St�phanie Plum, tome 18 : Y'a pas 18 solutions par Evanovich\"\n"
    "							title=\"St�phanie Plum, tome 18 : Y'a pas 18 solutions par Evanovich\"\n"
    "							onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "						<h2 itemprop=\"name\" style=\"	overflow:hidden; padding-bottom:2px;padding-top:2px;\">St�phanie\n"
    "							Plum, tome 18 : Y'a ..</h2>\n"
    "					</a><a href=\"/auteur/Janet-Evanovich/13390\">\n"
    "						<h3 style=\"margin:0px\">Janet Evanovich</h3>\n"
    "					</a>\n"
    "					<h3 style=\"margin:5px 0 0 0\"></h3>\n"
    "				</div>\n"
    "<div class=\"col col-4 list_livre\" style=\"max-width:120px;\" itemscope itemtype=\"http://schema.org/Book\">\n "
    "					<a href=\"/livres/Bowen-Son-Espionne-royale-et-le-mystere-bavarois-tome-2/1149567\"><img\n"
    "							loading=\"lazy\"\n"
    "							src=\"https://images-eu.ssl-images-amazon.com/images/I/51ZWXoW%2BZ2L._SX95_.jpg\"\n"
    "							alt=\"Son Espionne royale et le myst�re bavarois, tome 2 par Bowen\"\n"
    "							title=\"Son Espionne royale et le myst�re bavarois, tome 2 par Bowen\"\n"
    "							onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "						<h2 itemprop=\"name\" style=\"	overflow:hidden; padding-bottom:2px;padding-top:2px;\">Son Espionne\n"
    "							royale et le my..</h2>\n"
    "					</a><a href=\"/auteur/Rhys-Bowen/459795\">\n"
    "						<h3 style=\"margin:0px\">Rhys Bowen</h3>\n"
    "					</a>\n"
    "					<h3 style=\"margin:5px 0 0 0\"></h3>\n"
    "				</div>\n"
    "<div class=\"col col-4 list_livre\" style=\"max-width:120px;\" itemscope itemtype=\"http://schema.org/Book\">\n "
    "					<a href=\"/livres/Bussi-Sang-Famille/238989\"><img loading=\"lazy\"\n"
    "							src=\"https://images-eu.ssl-images-amazon.com/images/I/41aq%2BnCJXsL._SX95_.jpg\"\n"
    "							alt=\"Sang Famille par Bussi\" title=\"Sang Famille par Bussi\"\n"
    "							onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "						<h2 itemprop=\"name\" style=\"	overflow:hidden; padding-bottom:2px;padding-top:2px;\">Sang Famille\n"
    "						</h2>\n"
    "					</a><a href=\"/auteur/Michel-Bussi/113715\">\n"
    "						<h3 style=\"margin:0px\">Michel Bussi</h3>\n"
    "					</a>\n"
    "					<h3 style=\"margin:5px 0 0 0\"></h3>\n"
    "				</div>\n"
    "<div class=\"col col-4 list_livre\" style=\"max-width:120px;\" itemscope itemtype=\"http://schema.org/Book\">\n "
    "					<a href=\"/livres/Ceresa-Petit-papa-Nol/391598\"><img loading=\"lazy\"\n"
    "							src=\"/couv/sm_cvt_Petit-papa-Nol_6204.jpg\" alt=\"Petit papa No�l par C�r�sa\"\n"
    "							title=\"Petit papa No�l par C�r�sa\"\n"
    "							onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "						<h2 itemprop=\"name\" style=\"	overflow:hidden; padding-bottom:2px;padding-top:2px;\">Petit papa\n"
    "							No�l</h2>\n"
    "					</a><a href=\"/auteur/Francois-Ceresa/16785\">\n"
    "						<h3 style=\"margin:0px\">Fran�ois C�r�sa</h3>\n"
    "					</a>\n"
    "					<h3 style=\"margin:5px 0 0 0\"></h3>\n"
    "				</div>\n"
    "			</div>\n"
    "			<div class=\"sep\"></div>\n"
    "		</div>\n"
    "		<div class=\"side_r_content\">\n"
    "			<div class=\"titre\">\n"
    "				Les Derni�res Actualit�s\n"
    "				<a href=\"/decouvrir_actualites.php\" class=\"more\"><span></span>Voir plus</a></div>\n"
    "			<div class=\"list_article col\"><a\n"
    "					href=\"https://www.babelio.com/livres/Ducoudray-Monsieur-Jules/1166022/extraits\">\n"
    "					<div class=\" \"><img src=\"/users/Decouvrez-les-premieres-pages-de-Monsieur-Jules_S.jpg\">\n"
    "						<h2\n"
    "style=\"font-family: 'Roboto Slab',serif;    font-size: .8rem;    line-height: "
    ".85rem;margin-top:.7em;letter-spacing: -.5px;\">\n "
    "							D�couvrez les premi�res pages de Monsieur Jules</h2>\n"
    "				</a></div>\n"
    "		</div>\n"
    "		<div class=\"list_article col\"><a href=\"https://www.babelio.com/rencontre-na2\">\n"
    "				<div class=\" \"><img src=\"/users/Rencontrez-les-auteurs-de-Nouveaux-Auteurs_S.jpg\">\n"
    "					<h2\n"
    "style=\"font-family: 'Roboto Slab',serif;    font-size: .8rem;    line-height: "
    ".85rem;margin-top:.7em;letter-spacing: -.5px;\">\n "
    "						Rencontrez les auteurs de Nouveaux Auteurs�</h2>\n"
    "			</a></div>\n"
    "	</div>\n"
    "	<div class=\"list_article col\"><a href=\"https://www.babelio.com/rencontre-christophe-tison\">\n"
    "			<div class=\" \"><img src=\"/users/La-voix-de-Lolita--rencontrez-Christophe-Tison_S.jpg\">\n"
    "				<h2\n"
    "style=\"font-family: 'Roboto Slab',serif;    font-size: .8rem;    line-height: "
    ".85rem;margin-top:.7em;letter-spacing: -.5px;\">\n "
    "					La voix de Lolita : rencontrez Christophe Tison</h2>\n"
    "		</a></div>\n"
    "	</div>\n"
    "	</div>\n"
    "	<div class=\"side_r_content\">\n"
    "		<div class=\"titre\">\n"
    "			Autres livres de Juliette Sachs (1)\n"
    "			<a href=\"/auteur/Juliette-Sachs/498755/bibliographie\" class=\"more\"><span></span>Voir plus</a></div>\n"
    "		<div class=\"list_livre_con row\">\n"
    "<div class=\"col col-4 list_livre\" style=\"max-width:120px;\" itemscope itemtype=\"http://schema.org/Book\"><a\n "
    "					href=\"/livres/Sachs-On-nattire-pas-les-hirondelles-avec-du-vinaigre/1113637\"><img loading=\"lazy\"\n"
    "						src=\"https://images-eu.ssl-images-amazon.com/images/I/41J3ORlpMDL._SX95_.jpg\"\n"
    "						alt=\"On n'attire pas les hirondelles avec du vinaigre par Sachs\"\n"
    "						title=\"On n'attire pas les hirondelles avec du vinaigre par Sachs\"\n"
    "						onError=\"this.onerror=null;this.src='/images/couv-defaut.jpg';\" />\n"
    "					<h2 itemprop=\"name\" style=\"	overflow:hidden; padding-bottom:2px;padding-top:2px;\">On n'attire pas\n"
    "						les hirondel..</h2>\n"
    "				</a>\n"
    "				<h3 style=\"margin:5px 0 0 0\">\n"
    "					<nobr><a href=\"/livres/Sachs-On-nattire-pas-les-hirondelles-avec-du-vinaigre/1113637#critiques\"\n"
    "							class=\"titre_livre_elements\" style=\"color: #7F7F7F;\"><strong>5</strong>\n"
    "							critiques </a></nobr> &nbsp;\n"
    "					<nobr><a href=\"/livres/Sachs-On-nattire-pas-les-hirondelles-avec-du-vinaigre/1113637#citations\"\n"
    "							class=\"titre_livre_elements\" style=\"color: #7F7F7F;\"><strong>10 </strong>\n"
    "							citations</a></nobr>\n"
    "				</h3>\n"
    "			</div>\n"
    "		</div>\n"
    "	</div>\n"
    "	<div class=\"sep\"></div><br>\n"
    "	<div class=\"side_r_content\">\n"
    "		<div class=\"titre\">\n"
    "			Lecteurs (65)\n"
    "<a rel=\"subsection\" href=\"/connection.php\" class=\"more\"><span style=\"width: 76px;\"></span>Voir plus</a>\n "
    "		</div>\n"
    "		<div class=\"list_trombi\">\n"
    "			<div class=\"col col-4\"><a href=\"/monprofil.php?id_user=698030\" rel=\"nofollow\">\n"
    "					<div class=\"list_trombi_img\"><img src=\"/users/698030_a4123.jpg\"></div>\n"
    "					<h2>Emilyvale...</h2>\n"
    "				</a></div>\n"
    "			<div class=\"col col-4\"><a href=\"/monprofil.php?id_user=831815\" rel=\"nofollow\">\n"
    "					<div class=\"list_trombi_img\"><img src=\"/images/tete_v2_3.jpg\"></div>\n"
    "					<h2>Sophie_95</h2>\n"
    "				</a></div>\n"
    "			<div class=\"col col-4\"><a href=\"/monprofil.php?id_user=61263\" rel=\"nofollow\">\n"
    "					<div class=\"list_trombi_img\"><img src=\"/users/61263_a1159.jpg\"></div>\n"
    "					<h2>zaurie</h2>\n"
    "				</a></div>\n"
    "		</div><br>\n"
    "	</div>\n"
    "	<div class=\"sep\"></div><br>\n"
    "	<div id='div-gpt-ad-1442725480364-1' style=\"text-align:center;\">\n"
    "		<script type='text/javascript'>\n"
    "			googletag.cmd.push(function () { googletag.display('div-gpt-ad-1442725480364-1'); });\n"
    "		</script><br><br></div>\n"
    "	<div class=\"side_r_content\">\n"
    "		<div class=\"titre\">\n"
    "			Auteurs proches de Juliette Sachs\n"
    "\n"
    "		</div>\n"
    "		<div class=\"list_trombi\">\n"
    "			<div class=\"col col-4\" style=\"min-width:85px\"><a href=\"/auteur/Julia-Chapman/465165\" rel=\"nofollow\">\n"
    "					<div class=\"list_trombi_aut_img\"><img src=\"/users/avt_fd_465165.jpg\">\n"
    "						<h2 style=\"min-width:85px\">Julia Chapman</h2>\n"
    "					</div>\n"
    "				</a></div>\n"
    "			<div class=\"col col-4\" style=\"min-width:85px\"><a href=\"/auteur/Frederic-Dard/7187\" rel=\"nofollow\">\n"
    "					<div class=\"list_trombi_aut_img\"><img src=\"/users/avt_fd_7187_1552493457.jpg\">\n"
    "						<h2 style=\"min-width:85px\">Fr�d�ric Dard</h2>\n"
    "					</div>\n"
    "				</a></div>\n"
    "<div class=\"col col-4\" style=\"min-width:85px\"><a href=\"/auteur/Charles-Exbrayat/12086\" rel=\"nofollow\">\n "
    "					<div class=\"list_trombi_aut_img\"><img src=\"/users/avt_fd_12086.jpg\">\n"
    "						<h2 style=\"min-width:85px\">Charles Exbrayat</h2>\n"
    "					</div>\n"
    "				</a></div>\n"
    "			<div class=\"col col-4\" style=\"min-width:85px\"><a href=\"/auteur/Patrice-Dard/4391\" rel=\"nofollow\">\n"
    "					<div class=\"list_trombi_aut_img\"><img src=\"/users/avt_fd_4391.jpg\">\n"
    "						<h2 style=\"min-width:85px\">Patrice Dard</h2>\n"
    "					</div>\n"
    "				</a></div>\n"
    "			<div class=\"col col-4\" style=\"min-width:85px\"><a href=\"/auteur/MC-Beaton/262506\" rel=\"nofollow\">\n"
    "					<div class=\"list_trombi_aut_img\"><img src=\"/users/avt_fd_262506_1542803641.jpg\">\n"
    "						<h2 style=\"min-width:85px\">M.C. Beaton</h2>\n"
    "					</div>\n"
    "				</a></div>\n"
    "			<div class=\"col col-4\" style=\"min-width:85px\"><a href=\"/auteur/Sophie-Henaff/343029\" rel=\"nofollow\">\n"
    "					<div class=\"list_trombi_aut_img\"><img src=\"/users/avt_fd_343029.jpg\">\n"
    "						<h2 style=\"min-width:85px\">Sophie H�naff</h2>\n"
    "					</div>\n"
    "				</a></div>\n"
    "		</div>\n"
    "	</div>\n"
    "	<div class=\"sep\"></div>\n"
    "	<style media=\"screen\" type=\"text/css\">\n"
    "		.btn_radio_select:hover {\n"
    "			color: #000;\n"
    "		}\n"
    "\n"
    "		.btn_radio:hover {\n"
    "			color: #FFBC23;\n"
    "		}\n"
    "	</style>\n"
    "	<div class=\"side_r_content\">\n"
    "		<div class=\"titre\">\n"
    "			Quiz\n"
    "			<a href=\"/quiz/\" class=\"more\"><span></span>Voir plus</a></div>\n"
    "		<div class=\"side_quizz\">\n"
    "			<h2>Compl�ter les titres</h2>\n"
    "			<p>Orgueil et ..., de Jane Austen ?</p><a href=\"/quiz/110/Completer-les-titres\" class=\"\">\n"
    "				<div class=\"btn_radio_select\">\n"
    "					<div class=\"btn_radio\" style=\" \">Modestie</div>\n"
    "					<div class=\"btn_radio\" style=\" \">Vantardise</div>\n"
    "					<div class=\"btn_radio\" style=\" \">Innocence</div>\n"
    "					<div class=\"btn_radio\" style=\" \">Pr�jug�</div>\n"
    "				</div>\n"
    "			</a><br>\n"
    "			<div class=\"side_quizz_nb\">10 questions<br>\n"
    "				15254 lecteurs ont r�pondu\n"
    "			</div>\n"
    "\n"
    "			Th�mes :\n"
    "			<a class=\"tc_1 theme\" target=\"_blank\" href=\"/livres-/humour/15\">humour</a><a href=\"/ajout_quiz.php\"\n"
    "				class=\"tiny_links\">Cr�er un quiz sur ce livre</a>\n"
    "		</div>\n"
    "	</div>\n"
    "	<div class=\"sep\"></div>\n"
    "	</div>\n"
    "	</div>\n"
    "	<div id='div-gpt-ad-1442725480364-0' style=\"z-index:5;margin:auto;text-align:center;position:relative;\">\n"
    "		<script type='text/javascript'>\n"
    "			googletag.cmd.push(function () { googletag.display('div-gpt-ad-1442725480364-0'); });\n"
    "		</script>\n"
    "	</div>\n"
    "	</div>\n"
    "	<footer style=\"height:100%\">\n"
    "		<div class=\"col-4 col footer_col\">\n"
    "			<div class=\"footer_titre\">Navigation</div><a href=\"/aide.php\" rel=\"nofollow\"\n"
    "				class=\"col col-4 footer_links\">Aide</a><a href=\"/partenariats_editeurs.php\" rel=\"nofollow\"\n"
    "				class=\"col col-4 footer_links\">Publicit�</a><a href=\"/massecritique.php\"\n"
    "				class=\"col col-4 footer_links\">Masse critique</a><a href=\"https://babelio.freshdesk.com/support/home\"\n"
    "				rel=\"nofollow\" target=\"_blank\" class=\"col col-4 footer_links\">Contact</a><a\n"
    "				href=\"https://www.babeltheque.fr\" target=\"blank\" rel=\"nofollow\" class=\"col col-4 footer_links\"\n"
    "				target=\"_blank\">Babelth�que</a><a href=\"/partenaires.php\" rel=\"nofollow\"\n"
    "				class=\"col col-4 footer_links\">Sites Partenaires</a><a href=\"https://babelio.wordpress.com/\"\n"
    "				class=\"col col-4 footer_links\" target=\"_blank\">Blog</a><a href=\"/apropos.php\" rel=\"nofollow\"\n"
    "				class=\"col col-4 footer_links\">A propos</a><a href=\"http://www.cinetrafic.fr\" target=\"_blank\"\n"
    "				rel=\"nofollow\" class=\"col col-4 footer_links\">Listes de films</a><a\n"
    "				href=\"https://sites.google.com/site/defibabelio/home\" target=\"_blank\" rel=\"nofollow\"\n"
    "				class=\"col col-4 footer_links\">D�fi Babelio</a>\n"
    "		</div>\n"
    "		<div class=\"col-4 col footer_col\">\n"
    "			<div class=\"footer_titre\" align=\"center\">Suivez-nous</div>\n"
    "<div class=\"footer_icons\"><a href=\"/rss_critiques.php\" rel=\"nofollow\" target=\"_blank\" target=\"_blank\"\n "
    "					class=\"icon-blogger\"></a><a href=\"https://www.facebook.com/pages/Babeliocom/36218830677\"\n"
    "					rel=\"nofollow\" target=\"_blank\" class=\"icon-blogger4\"></a><a href=\"https://www.twitter.com/babelio\"\n"
    "					class=\"icon-blogger5\" target=\"_blank\"></a></div>\n"
    "		</div>\n"
    "<div class=\"col-4 col footer_col\"><a href=\"https://itunes.apple.com/fr/app/babelio/id1449315145?l=fr&ls=1&mt"
    "=8\"\n "
    "				target=\"_blank\" rel=\"nofolliow\"><img src=\"/images/relais_app_apple.png\"></a><a\n"
    "				href=\"https://play.google.com/store/apps/details?id=com.babelio.babelio&hl=fr\" target=\"_blank\"\n"
    "				rel=\"nofolliow\"><img src=\"/images/relais_app_google.png\"></a></div>\n"
    "		<div class=\"col-4 col footer_col\">\n"
    "			<div id=\"back-to-top\">retour en haut de page\n"
    "\n"
    "				<svg viewBox=\"0 0 595.279 841.891\">\n"
    "<path fill=\"none\" d=\"M581.864,500.842l-31.371,31.371l-249.81-249.839L49.832,532.213L18.52,"
    "504.752l283.672-278.441\n "
    "            L581.864,500.842z\" /></svg></div>\n"
    "		</div>\n"
    "		<div class=\"footer_bottom row\">\n"
    "			<div>� BABELIO - 2007-2018</div><span></span>\n"
    "		</div>\n"
    "	</footer>\n"
    "	</div>\n"
    "	</div>\n"
    "	<script src=\"/js_cache/1,34,2,3,4,5,9,21,19,28,32,noJQ,35,36,37,38,39,48,47,__45\" type=\"text/javascript\"\n"
    "		charset=\"ISO-8859-1\"></script>\n"
    "	<div class=\"cookie-message\">\n"
    "<p class=\"cookie-content\">Les cookies assurent le bon fonctionnement de Babelio. En poursuivant la navigation\n "
    "			vous en acceptez le fonctionnement\n"
    "			&nbsp; <button class=\"btn inline\" style=\"padding: 8px;\" id=\"cookie_ok\">OK</button> &nbsp; <nobr><a\n"
    "					href=\"/cookies.php\" style=\"color: #FF9D38\" target=\"_blank\">En savoir plus</a></nobr>\n"
    "		</p>\n"
    "	</div>\n"
    "	<script type=\"text/javascript\" language=\"javascript\">\n"
    "		$(\"#cookie_ok\").on('click', function () {//#cookie stop is the Id of the button that closes the disclaimer\n"
    "\n"
    "			$('.cookie-message').slideUp(); //#cookie_disclaimer is the Id of the cookie disclaimer container\n"
    "\n"
    "			//here i set the cookie variable \"disclaimer\" to true so i can check if the user \n"
    "			var nDays = 999;\n"
    "			var cookieName = \"disclaimer\";\n"
    "			var cookieValue = \"true\";\n"
    "\n"
    "			var today = new Date();\n"
    "			var expire = new Date();\n"
    "			expire.setTime(today.getTime() + 3600000 * 24 * nDays);\n"
    "document.cookie = cookieName + \"=\" + escape(cookieValue) + \";expires=\" + expire.toGMTString() + "
    "\";path=/\";\n "
    "			//done with the cookie\n"
    "		});\n"
    "	</script>\n"
    "	<style media=\"screen\" type=\"text/css\">\n"
    "		.cookie-message {\n"
    "			width: 100%;\n"
    "			background: #333;\n"
    "			color: #ddd;\n"
    "			padding: .15em;\n"
    "			position: fixed;\n"
    "			text-align: center;\n"
    "			bottom: 0;\n"
    "			z-index: 99999;\n"
    "		}\n"
    "\n"
    "		.cookie-content {\n"
    "			margin: auto;\n"
    "			text-align: center;\n"
    "		}\n"
    "	</style>\n"
    "	<script language=javascript>\n"
    "\n"
    "\n"
    "\n"
    "		$('.rateit').bind('rated reset', function (e) {\n"
    "			var ri = $(this);\n"
    "\n"
    "\n"
    "			var value = ri.rateit('value');\n"
    "			var ID = ri.data('id');\n"
    "			var table = ri.data('table');\n"
    "			var sel = ri.data('sel');\n"
    "			//alert(value + ' ' + ID + ' ' + table );\n"
    "\n"
    "\n"
    "\n"
    "			//maybe we want to disable voting?\n"
    "			//ri.rateit('readonly', true);\n"
    "\n"
    "			$.ajax({\n"
    "				url: '/aj_rating_rpc.php', //your server side script\n"
    "				data: { id: ID, value: value, table: table, sel: sel }, //our data\n"
    "				type: 'POST',\n"
    "				success: function (data) {\n"
    "					//$('#response').append('<li>' + data + '</li>');\n"
    "					//alert(data);\n"
    "\n"
    "				},\n"
    "				error: function (jxhr, msg, err) {\n"
    "\n"
    "				}\n"
    "			});\n"
    "\n"
    "		});\n"
    "	</script>\n"
    "	<script type=\"text/javascript\" language=\"javascript\">\n"
    "\n"
    "		var aj_cit_lb = function (idTag, mode) {\n"
    "$('#lightbox_con').html('<iframe height=\"100%\" width=\"100%\" frameborder=\"0\" "
    "src=\"/aj_cit_lb.php?id_oeuvre=1166729&id_user=&id_auteur_canonique=498755\"></iframe>');\n "
    "			TweenMax.to('#lightbox_overlay', 1, { autoAlpha: 0.7, ease: Expo.easeOut });\n"
    "			TweenMax.set('#lightbox', { y: -2000 });\n"
    "			TweenMax.to('#lightbox', 0.5, { y: 0, autoAlpha: 1, ease: Expo.easeOut });\n"
    "		}\n"
    "		$('.cls_aj_cit').click(function () {\n"
    "			aj_cit_lb();\n"
    "		});\n"
    "\n"
    "		var aj_cri_lb = function (idTag, mode) {\n"
    "$('#lightbox_con').html('<iframe height=\"100%\" width=\"100%\" frameborder=\"0\" "
    "src=\"/aj_cri_lb.php?id_oeuvre=1166729&id_user=\"></iframe>');\n "
    "			TweenMax.to('#lightbox_overlay', 1, { autoAlpha: 0.7, ease: Expo.easeOut });\n"
    "			TweenMax.set('#lightbox', { y: -2000 });\n"
    "			TweenMax.to('#lightbox', 0.5, { y: 0, autoAlpha: 1, ease: Expo.easeOut });\n"
    "		}\n"
    "		$('.cls_aj_cri').click(function () {\n"
    "			aj_cri_lb();\n"
    "		});\n"
    "\n"
    "\n"
    "	</script><span></span>\n"
    "	<script>\n"
    "		(function (i, s, o, g, r, a, m) {\n"
    "		i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {\n"
    "			(i[r].q = i[r].q || []).push(arguments)\n"
    "		}, i[r].l = 1 * new Date(); a = s.createElement(o),\n"
    "			m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)\n"
    "		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');\n"
    "\n"
    "		ga('create', 'UA-1000343-2', 'auto');  // Replace with your property ID.\n"
    "		ga('send', 'pageview');\n"
    "\n"
    "	</script>\n"
    "</body>\n"
    "\n"
    "</html>")

GET_INFO_HTML = b'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" ' \
                b'"https://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html version="XHTML+RDFa 1.0" ' \
                b'xmlns="https://www.w3.org/1999/xhtml" xml:lang="fr"><head><meta http-equiv="Content-Type" ' \
                b'content="text/html; charset=iso-8859-1" /><meta http-equiv="Content-language" content="fr-FR" ' \
                b'/><meta http-equiv="cache-control" content="no-cache"><meta http-equiv="pragma" ' \
                b'content="no-cache"><meta http-equiv="expires" content="-1"><link rel="SHORTCUT ICON" ' \
                b'href="/faviconbabelio_.ico" /><meta charset="iso-8859-1"><meta http-equiv="X-UA-Compatible" ' \
                b'content="IE=edge"><meta name="viewport" content="user-scalable=no, width=device-width, ' \
                b'initial-scale=1, maximum-scale=1"><meta property="og:type" content="website"></meta><title>Babelio ' \
                b'- D\xe9couvrez des livres, critiques, extraits, r\xe9sum\xe9s</title><meta name="title" ' \
                b'content="Babelio - D\xe9couvrez des livres, critiques, extraits, r\xe9sum\xe9s" /><meta ' \
                b'name="description" content="Le site o\xf9 les passionn\xe9s de lecture partagent et \xe9changent ' \
                b'autour de leurs lectures" /><meta name="keywords" content="babelio, livre, livres en ligne, ' \
                b'bibliotheque en ligne, critiques livres, classer livres, logiciel gestion bibliotheque, ' \
                b'livre occasion, livre photo, livre enfant, livre ancien, vente livre, livre scolaire, ' \
                b'litt\xe9rature, litterature, bandes dessin\xe9es, bande dessinee, bd, contes, recette de cuisine, ' \
                b'dictionnaire, dictionnaire anglais francais, dictionnaire francais, dictionnaire des synonymes, ' \
                b'dictionnaire anglais, mangas, mangas x, jeunesse, policier, roman policier, polar, machefer, roman, ' \
                b'harry potter, asterix, tintin, star wars, point de croix, philosophie, atlas, art, prix goncourt, ' \
                b'science fiction, poesie, livre poche, pleiade, tourisme, histoire erotique, histoire, ' \
                b'lecture" /><link rel="stylesheet" type="text/css" href="/css_cache/17,18,20,21__45.css" ' \
                b'media="all"/><link rel="canonical" href="https://www.babelio.com/livrespopulaires_debut.php" ' \
                b'/><meta property="og:url" content="https://www.babelio.com/livrespopulaires_debut.php"><script ' \
                b'type=\'text/javascript\'>\nvar yieldlove_site_id = "babelio.com_deconnecte";\n</script><script ' \
                b'type=\'text/javascript\' src=\'//cdn-a.yieldlove.com/yieldlove-bidder.js?babelio.com_deconnecte' \
                b'\'></script><script type="text/javascript">\n \t\tvar habillage_state = 0;\n\t  var googletag = ' \
                b'googletag || {};\n\t  googletag.cmd = googletag.cmd || [];\n\t  (function() {\n\t    var gads = ' \
                b'document.createElement("script");\n\t    gads.async = true;\n\t    gads.type = "text/javascript";\n ' \
                b'\t    var useSSL = "https:" == document.location.protocol;\n \t    gads.src = (useSSL ? "https:" : ' \
                b'"http:") + "//www.googletagservices.com/tag/js/gpt.js";\n \t    var node ' \
                b'=document.getElementsByTagName("script")[0];\n \t    node.parentNode.insertBefore(gads, node);\n \t ' \
                b'  })();\n \t</script><script type=\'text/javascript\'>\n  googletag.cmd.push(function() {\n  \t\n  ' \
                b'\tgoogletag.pubads().setTargeting(\'habillage\', "2");\n    googletag.pubads().setTargeting(' \
                b'\'campagnes_pub_params\', [""]);\n    googletag.pubads().setTargeting(\'home\', "");\n\n/*\nvar ' \
                b'mapping_habillage = googletag.sizeMapping().\n\taddSize([0, 0], []).\n  addSize([480, 0], [480,' \
                b'190]). // tel\n  addSize([768, 0], [768,190]). // Tablette\n  addSize([1024, 0],  [0,' \
                b'0]). // Ordinateur de bureau\n  build();\n    \n    googletag.defineOutOfPageSlot(' \
                b'\'/1032445/habillage_site\', \'div-gpt-ad-1442608793107-0\').\n   \tdefineSizeMapping(' \
                b'mapping_habillage).\n \t\tsetCollapseEmptyDiv(true).  \n    addService(googletag.pubads()); \n   \n ' \
                b'  */ \n      \n      googletag.defineOutOfPageSlot(\'/1032445/habillage_site\', ' \
                b'\'div-gpt-ad-1442608793107-0\').addService(googletag.pubads()); \n      \n    \n\tvar ' \
                b'mapping_meg_haut = googletag.sizeMapping().\n  addSize([0, 0], [[300,250], [320,100]]).\n  addSize(' \
                b'[480, 0], [320,100]). // tel\n  addSize([768, 0], [728,90]). // Tablette\n  addSize([1024, 0],  ' \
                b'[[728, 90], [970, 90], [1000, 90]]). // Ordinateur de bureau\n  build();\n          \n    ' \
                b'\tgoogletag.defineSlot(\'/1032445/megabann_haut\',  [[320, 100], [728, 90], [970, 90], [1000, 90]], ' \
                b'\'div-gpt-ad-1442725206084-0\').\n \t\t defineSizeMapping(mapping_meg_haut).\n    \taddService(' \
                b'googletag.pubads());\n        \n    \n\t\t\tvar mapping_pave_bas = googletag.sizeMapping().\n\t\t  ' \
                b'addSize([0, 0], [300, 250]).\n\t\t  addSize([1024, 0],  [[300, 600], [300, 250]]). // Ordinateur de ' \
                b'bureau\n\t\t  build();    \n    \n    \tgoogletag.defineSlot(\'/1032445/pave_bas\', [[300, 600], ' \
                b'[300, 250]], \'div-gpt-ad-1442725480364-1\').\n \t\t\tdefineSizeMapping(mapping_pave_bas).\n \t\t ' \
                b'\tsetCollapseEmptyDiv(true).    \t\n    \taddService(googletag.pubads());\n  \n  \n  \tvar ' \
                b'mapping_meg_bas = googletag.sizeMapping().\n\t  addSize([0, 0], [[300,250], [320,100]]).\n\t  ' \
                b'addSize([480, 0], [[300,250], [320,100]]). // tel\n\t  addSize([768, 0], [728,90]). // Tablette\n\t ' \
                b' addSize([1024, 0],  [[728, 90], [970, 90]]). // Ordinateur de bureau\n\t  build();\n\t    \t\n    ' \
                b'\tgoogletag.defineSlot(\'/1032445/megabann_bas\', [728, 90], \'div-gpt-ad-1442725480364-0\').\n    ' \
                b'\tdefineSizeMapping(mapping_meg_bas).\n \t\t \tsetCollapseEmptyDiv(true).    \t\n    \taddService(' \
                b'googletag.pubads());\n    \t\n    \n\t\t\tvar mapping_pave_haut = googletag.sizeMapping().\n\t\t  ' \
                b'addSize([0, 0], [300, 250]).\n\t\t  addSize([1024, 0],  [[300, 600], [300, 250]]). // Ordinateur de ' \
                b'bureau\n\t\t  build();  \n\t\t      \n    \tgoogletag.defineSlot(\'/1032445/pave_haut\', [[300, ' \
                b'600], [300, 250]], \'div-gpt-ad-1442725480364-2\').\n    \tdefineSizeMapping(mapping_pave_haut).\n ' \
                b'\t\t \tsetCollapseEmptyDiv(true).    \t\n    \taddService(googletag.pubads());\n\n\tvar ' \
                b'mapping_pave_responsive_haut = googletag.sizeMapping().\n  addSize([0, 0], [[320, 100], [300,' \
                b'250]]).\n  addSize([480, 0], [300, 250]). // Tablette\n  addSize([1024, 0], []). // Ordinateur de ' \
                b'bureau\n  build();\n      \n\t\tgoogletag.defineSlot(\'/1032445/pave_responsive_haut\', [[300, ' \
                b'250], [300, 100]], \'div-gpt-ad-1465403049525-0\').\n\t\t defineSizeMapping(' \
                b'mapping_pave_responsive_haut).\n  \tsetCollapseEmptyDiv(true).\n\t\taddService(googletag.pubads(' \
                b'));\n\n    googletag.pubads().enableSingleRequest();\n    googletag.pubads().collapseEmptyDivs();\n ' \
                b'   //googletag.pubads().enableSyncRendering();\n    googletag.enableServices();\n    ' \
                b'googletag.pubads().addEventListener(\'slotRenderEnded\', function(event) { \n    if (' \
                b'event.slot.getSlotElementId() == "div-gpt-ad-1442608793107-0") { \n    \tif (!event.isEmpty)\n    ' \
                b'\t\t{\n\t\t\t\t\tSetHabillage();\n  \t\t\t}\n    }\n   \n});\n\t\n  });\n</script><link ' \
                b'rel="dns-prefetch" href="//fonts.googleapis.com"><link rel="dns-prefetch" ' \
                b'href="https://connect.facebook.net"><link rel="prefetch" href="/images/spritesheet.png"><link ' \
                b'rel="dns-prefetch" href="//google-analytics.com"><link rel="dns-prefetch" ' \
                b'href="//www.google-analytics.com"><link rel="dns-prefetch" ' \
                b'href="http://www.googletagservices.com"><link rel="dns-prefetch" ' \
                b'href="http://partner.googleadservices.com"></head><body onunload="" ><div ' \
                b'id=\'div-gpt-ad-1442608793107-0\' style="z-index:1;"><script ' \
                b'type=\'text/javascript\'>\ngoogletag.cmd.push(function() { googletag.display(' \
                b'\'div-gpt-ad-1442608793107-0\'); });\n</script></div><input type="hidden" id="hid_version" ' \
                b'value="3"><div id="overlay"></div><div id="lightbox_overlay"></div><div id="lightbox"><div ' \
                b'id="lightbox_close" class="tiny_links dark">fermer</div><div id="lightbox_con"></div></div><div ' \
                b'id="mobile_menu" ><a href="/actualites.php" rel="nofollow" id="bleb4i">Accueil</a><a ' \
                b'href="/mabibliotheque.php" rel="nofollow">Mes livres</a><a href="/ajoutlivres.php" ' \
                b'rel="nofollow">Ajouter des livres</a><div class="sep"></div><div class="titre" ' \
                b'>D\xe9couvrir</div><a href="/decouvrir.php">Livres</a><a href="/decouvrirauteurs.php">Auteurs</a><a ' \
                b'href="/decouvrirmembres.php" >Lecteurs</a><a href="/dernierescritiques.php">Critiques</a><a ' \
                b'href="/dernierescitations.php">Citations</a><a href="/listes-de-livres/">Listes</a><a ' \
                b'href="/quiz/">Quiz</a><a href="/groupes" rel="no-follow">Groupes</a><a href="/questions" ' \
                b'rel="no-follow">Questions</a><a href="/prix-babelio" rel="no-follow">Prix Babelio</a></div><div ' \
                b'id="mobile_user_menu" ></div><style media="screen" type="text/css">\n@media (min-width: 1025px) \n{' \
                b'\n.logo_svg { \n    width: 171px;\n    margin: 25px 15px 20px 10px;\n    float: ' \
                b'left;\n\n}\n\nheader .menu .menu_item_decouvrir_con \n{\n\tmargin-left:5px;\n}\n}\n\n@media (' \
                b'max-width: 1024px) \n{\n.logo_svg { \n    margin: 10px 15px 10px 0;\n    width: 110px;\n    height: ' \
                b'41px;\n    float: left;\n}\n}\n</style><div id="wrapper" style=""><div class="top_facebook ' \
                b'only-desktop" id="bandeau_incitation">\n\t        Rejoignez Babelio pour d\xe9couvrir vos ' \
                b'prochaines lectures <a href="/facebook" rel="nofollow" class="top_facebook_login">connexion avec ' \
                b'<div></div></a><a href="/register.php" rel="nofollow" class="top_facebook_signin">Inscription ' \
                b'Classique</a></div><header class="row" id="page_head"><div id="mobile_menu_btn"><span ' \
                b'class="row1"></span><span class="row2"></span><span class="row3"></span></div><a href="/" ><img ' \
                b'src="/images/logo-babelio-header.png" alt="Babelio" srcset="/images/logo.svg" class="logo_svg" ' \
                b'></a><div class="header_search"><form enctype="multipart/form-data" action="/resrecherche.php" ' \
                b'method="post" id="form2" ><input type="text" class="input" placeholder="Rechercher" ' \
                b'name="Recherche" id="searchbox" /><button type="submit" name="recherche" ' \
                b'class="header_search_submit icon-blogger8"></button></form><a href="/recherche_avancee.php" ' \
                b'class="header_search_more">plus d\'options</a><div id="header_search_close" class=" ' \
                b'icon-blogger9"></div></div><div class="menu"><div class="menu_item menu_item_decouvrir_con "><div ' \
                b'id="menu_item_decouvrir_under" class="menu_under"><a href="/decouvrir.php">Livres</a><a ' \
                b'href="/decouvrirauteurs.php">Auteurs</a><a href="/decouvrirmembres.php" >Lecteurs</a><a ' \
                b'href="/dernierescritiques.php">Critiques</a><a href="/dernierescitations.php">Citations</a><a ' \
                b'href="/listes-de-livres/">Listes</a><a href="/quiz/">Quiz</a><a href="/groupes" ' \
                b'rel="no-follow">Groupes</a><a href="/questions" rel="no-follow">Questions</a><a ' \
                b'href="/prix-babelio" rel="no-follow">Prix Babelio</a></div><a href="/decouvrir.php" ' \
                b'id="menu_item_decouvrir" class=" menu_link">\n                    D\xe9couvrir\n                    ' \
                b'<div id="menu_item_decouvrir_arrow_1" class="icon-blogger7"></div><div ' \
                b'id="menu_item_decouvrir_arrow_2" class="icon-blogger7"></div></a></div><div ' \
                b'class="menu_item_anim"><div class="menu_item_sep"></div><a href="/actualites.php" rel="nofollow" ' \
                b'class="menu_item menu_link">Accueil<span></span></a><a href="/mabibliotheque.php" rel="nofollow" ' \
                b'class="menu_item menu_link">Mes livres<span></span></a><a href="/ajoutlivres.php" rel="nofollow" ' \
                b'class="menu_item menu_link">Ajouter des livres<span></span></a></div><div ' \
                b'class="menu_sep"></div><div class="menu_compte"><br><table><tr><td><a href="/facebook" ' \
                b'style="background: url(\'/images/spritesheet.png\') no-repeat scroll 0px -0px;    color: #FFF;    ' \
                b'padding: 0px;    display: block;width:20px;height:20px";></a></td><td><a href="/connection.php" ' \
                b'rel="nofollow" ><b>Connexion</b></a></td></tr></table><a href="/recupmdp.php" rel="nofollow" ' \
                b'style="font-size: 0.5rem;">mot de passe oubli\xe9 ?</a></div></div><div id="mobile_user_btn"><a ' \
                b'href="/connection.php" rel="nofollow"><div class="icon-enter"></div></a></div><div ' \
                b'id="mobile_search_btn" class=" icon-blogger8"></div></header><div class="corps row" id="page_corps" ' \
                b'style="z-index:2;position:relative;"><div class="content row"><div class="megaban" ' \
                b'id="megaban"><div id=\'div-gpt-ad-1442725206084-0\' ><script ' \
                b'type=\'text/javascript\'>\ngoogletag.cmd.push(function() { googletag.display(' \
                b'\'div-gpt-ad-1442725206084-0\'); });\n</script></div></div><div class="livre_header row"><a ' \
                b'href="/decouvrir.php" class="not-mobile"><img src="/images/decouvrir.jpg"/></a><div ' \
                b'class="livre_header_con"><h1>D\xe9couvrir</h1><div class="livre_header_links menu_item_anim" ' \
                b'style="width: 1000px;"><a href="/decouvrir.php" class="current menu_link">Livres<span></span></a><a ' \
                b'href="/a_paraitre.php" class=" menu_link">A para\xeetre<span></span></a><a href="/decouvrirn.php" ' \
                b'class=" menu_link">Livres (r\xe9cents)<span></span></a><a href="/decouvrirauteurs.php" class=" ' \
                b'menu_link">Auteurs<span></span></a><a href="/decouvrirmembres.php" class=" ' \
                b'menu_link">Lecteurs<span></span></a><a href="/dernierescritiques.php" class=" ' \
                b'menu_link">Critiques<span></span></a><a href="/dernierescitations.php" class=" ' \
                b'menu_link">Citations<span></span></a><a href="/derniersvideos.php" class=" ' \
                b'menu_link">Videos<span></span></a></div></div></div><div class="toolbar_top row"><a href="?p=1" ' \
                b'class="bd col-4" rel="nofollow" style="margin-right:2em;display:inline;">Cette semaine <span ' \
                b'class="icon-blogger7"></span></a>\n\t\t&nbsp; <a href="?p=2" class=" col-4" rel="nofollow" ' \
                b'style="margin-right:2em;display:inline;">Ce mois-ci <span ' \
                b'class="icon-blogger7"></span></a>\n\t\t&nbsp; <a href="?p=3" class=" col-4" rel="nofollow" ' \
                b'style="display:inline;">Depuis la nuit des temps <span class="icon-blogger7"></span></a></div><div ' \
                b'class="titre">Livres les plus populaires <a href="/decouvrir.php" ' \
                b'class="more"><span></span>Tous</a></div><div class="list_livre_con"><div class="col col-2-4 ' \
                b'list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Sachs-Vacances-mortelles-au-paradis/1166729" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/51DsFIlo3nL._SX95_.jpg" alt="Vacances mortelles ' \
                b'au paradis par Sachs" title="Vacances mortelles au paradis par Sachs" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Vacances mortelles au paradis</h2></a><a href="/auteur/Juliette-Sachs/498755"><h3 ' \
                b'style="margin:0px">Juliette Sachs</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Jullian-Le-berceau-du-Talion/1170670" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/314zuTOFEaL._SX95_.jpg" alt="Le berceau du ' \
                b'Talion par Jullian" title="Le berceau du Talion par Jullian" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Le ' \
                b'berceau du Talion</h2></a><a href="/auteur/Sebastien-Jullian/468623"><h3 ' \
                b'style="margin:0px">S\xe9bastien Jullian</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div ' \
                b'class="col col-2-4 list_livre" style="max-width:120px;" itemscope ' \
                b'itemtype="http://schema.org/Book"><a href="/livres/Piketty-Capital-et-ideologie/1167551" ><img ' \
                b'loading="lazy" src=\n\t\t  \t\t  \t"/couv/cvt_Capital-et-ideologie_4660.jpg" alt="Capital et ' \
                b'id\xe9ologie par Piketty" title="Capital et id\xe9ologie par Piketty" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Capital et id\xe9ologie</h2></a><a href="/auteur/Thomas-Piketty/87205"><h3 ' \
                b'style="margin:0px">Thomas Piketty</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Carrisi-Le-jeu-du-Chuchoteur/1169261" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/41V3alBD-PL._SX95_.jpg" alt="Le jeu du ' \
                b'Chuchoteur par Carrisi" title="Le jeu du Chuchoteur par Carrisi" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Le ' \
                b'jeu du Chuchoteur</h2></a><a href="/auteur/Donato-Carrisi/93832"><h3 style="margin:0px">Donato ' \
                b'Carrisi</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" ' \
                b'style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Snowden-Memoires-vives/1162306" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/51%2Bcx9NCv0L._SX95_.jpg" alt="M\xe9moires ' \
                b'vives  par Snowden" title="M\xe9moires vives  par Snowden" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">M\xe9moires vives </h2></a><a href="/auteur/Edward-Snowden/516723"><h3 ' \
                b'style="margin:0px">Edward Snowden</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Hamill-Une-cosmologie-de-monstres/1160843" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/51LKRP9eE6L._SX95_.jpg" alt="Une cosmologie de ' \
                b'monstres par Hamill" title="Une cosmologie de monstres par Hamill" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Une ' \
                b'cosmologie de monstres</h2></a><a href="/auteur/Shaun-Hamill/516108"><h3 style="margin:0px">Shaun ' \
                b'Hamill</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" ' \
                b'style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Liu-Boule-de-foudre/1169042" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/41ulQieFJ5L._SX95_.jpg" alt="Boule de foudre ' \
                b'par Liu" title="Boule de foudre par Liu" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Boule de foudre</h2></a><a href="/auteur/Cixin-Liu/340977"><h3 style="margin:0px">Cixin ' \
                b'Liu</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" ' \
                b'style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Becker-La-Maison/1149677" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/31HSuFH8vnL._SX95_.jpg" alt="La Maison par ' \
                b'Becker" title="La Maison par Becker" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">La ' \
                b'Maison</h2></a><a href="/auteur/Emma-Becker/119509"><h3 style="margin:0px">Emma Becker</h3></a><h3 ' \
                b'style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" style="max-width:120px;" ' \
                b'itemscope itemtype="http://schema.org/Book"><a href="/livres/Chabane-Mecanique-celeste/1155575" ' \
                b'><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"/couv/cvt_Mecanique-Celeste-tome-0-Mecanique-Celeste_3448.jpg" alt="M\xe9canique c\xe9leste par ' \
                b'Chabane" title="M\xe9canique c\xe9leste par Chabane" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">M\xe9canique c\xe9leste</h2></a><a href="/auteur/Merwan-Chabane/139230"><h3 ' \
                b'style="margin:0px">Merwan Chabane</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Vanistendael-Les-deux-vies-de-Penelope/1169984" ><img loading="lazy" src=\n\t\t  \t\t ' \
                b' \t"https://images-eu.ssl-images-amazon.com/images/I/41mC6JnCkiL._SX95_.jpg" alt="Les deux vies de ' \
                b'P\xe9n\xe9lope par Vanistendael" title="Les deux vies de P\xe9n\xe9lope par Vanistendael" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Les ' \
                b'deux vies de P\xe9n\xe9lope</h2></a><a href="/auteur/Judith-Vanistendael/86348"><h3 ' \
                b'style="margin:0px">Judith Vanistendael</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div ' \
                b'class="col col-2-4 list_livre" style="max-width:120px;" itemscope ' \
                b'itemtype="http://schema.org/Book"><a href="/livres/Pim-Tremen/1161106" ><img loading="lazy" ' \
                b'src=\n\t\t  \t\t  \t"https://images-eu.ssl-images-amazon.com/images/I/41D0smgaUvL._SX95_.jpg" ' \
                b'alt="Tremen par Pim" title="Tremen par Pim" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Tremen</h2></a><a href="/auteur/Bos-Pim/516226"><h3 style="margin:0px">Bos Pim</h3></a><h3 ' \
                b'style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" style="max-width:120px;" ' \
                b'itemscope itemtype="http://schema.org/Book"><a href="/livres/Beuglet-Lile-du-diable/1168709" ><img ' \
                b'loading="lazy" src=\n\t\t  \t\t  \t"/couv/cvt_Lile-du-diable_7556.jpg" alt="L\'\xeele du diable par ' \
                b'Beuglet" title="L\'\xeele du diable par Beuglet" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">L\'\xeele du diable</h2></a><a href="/auteur/Nicolas-Beuglet/405153"><h3 ' \
                b'style="margin:0px">Nicolas Beuglet</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Toussaint-La-cle-USB/1147662" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/316RBYF%2Bs0L._SX95_.jpg" alt="La cl\xe9 USB ' \
                b'par Toussaint" title="La cl\xe9 USB par Toussaint" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">La ' \
                b'cl\xe9 USB</h2></a><a href="/auteur/Jean-Philippe-Toussaint/3918"><h3 ' \
                b'style="margin:0px">Jean-Philippe Toussaint</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div ' \
                b'class="col col-2-4 list_livre" style="max-width:120px;" itemscope ' \
                b'itemtype="http://schema.org/Book"><a href="/livres/Tournay-Civilisation-00/1169308" ><img ' \
                b'loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/51OuNT5PlOL._SX95_.jpg" alt="Civilisation 0.0 ' \
                b'par Tournay" title="Civilisation 0.0 par Tournay" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Civilisation 0.0</h2></a><a href="/auteur/Virginie-Tournay/519483"><h3 ' \
                b'style="margin:0px">Virginie Tournay</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Crossan-Moon-brothers/1162622" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/41TekpU7JHL._SX95_.jpg" alt="Moon brothers par ' \
                b'Crossan" title="Moon brothers par Crossan" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Moon brothers</h2></a><a href="/auteur/Sarah-Crossan/368644"><h3 style="margin:0px">Sarah ' \
                b'Crossan</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" ' \
                b'style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Loubry-Les-refuges/1158683" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/51MtZqxbN4L._SX95_.jpg" alt="Les refuges par ' \
                b'Loubry" title="Les refuges par Loubry" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Les ' \
                b'refuges</h2></a><a href="/auteur/Jerome-Loubry/442819"><h3 style="margin:0px">J\xe9r\xf4me ' \
                b'Loubry</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" ' \
                b'style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Mizubayashi-me-brisee/1151329" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/41NdIhJJM4L._SX95_.jpg" alt="\xc2me bris\xe9e ' \
                b'par Mizubayashi" title="\xc2me bris\xe9e par Mizubayashi" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">\xc2me bris\xe9e</h2></a><a href="/auteur/Akira-Mizubayashi/119607"><h3 ' \
                b'style="margin:0px">Akira Mizubayashi</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div ' \
                b'class="col col-2-4 list_livre" style="max-width:120px;" itemscope ' \
                b'itemtype="http://schema.org/Book"><a href="/livres/Amigorena-Le-Ghetto-interieur/1156095" ><img ' \
                b'loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/41K2KM7tEVL._SX95_.jpg" alt="Le Ghetto ' \
                b'int\xe9rieur par Amigorena" title="Le Ghetto int\xe9rieur par Amigorena" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Le ' \
                b'Ghetto int\xe9rieur</h2></a><a href="/auteur/Santiago-H-Amigorena/232012"><h3 ' \
                b'style="margin:0px">Santiago H. Amigorena</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div ' \
                b'class="col col-2-4 list_livre" style="max-width:120px;" itemscope ' \
                b'itemtype="http://schema.org/Book"><a href="/livres/Tchakaloff-Vacarme/1156353" ><img loading="lazy" ' \
                b'src=\n\t\t  \t\t  \t"https://images-eu.ssl-images-amazon.com/images/I/3131dMw27cL._SX95_.jpg" ' \
                b'alt="Vacarme par Tchakaloff" title="Vacarme par Tchakaloff" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Vacarme</h2></a><a href="/auteur/Gal-Tchakaloff/391320"><h3 style="margin:0px">Ga\xebl ' \
                b'Tchakaloff</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" ' \
                b'style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Agrimbau-Lhumain/1158999" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/51H3n3rFwtL._SX95_.jpg" alt="L\'humain  par ' \
                b'Agrimbau" title="L\'humain  par Agrimbau" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">L\'humain </h2></a><a href="/auteur/Diego-Agrimbau/106944"><h3 style="margin:0px">Diego ' \
                b'Agrimbau</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" ' \
                b'style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Altan-Je-ne-reverrai-plus-le-monde--Textes-de-prison/1152662" ><img loading="lazy" ' \
                b'src=\n\t\t  \t\t  \t"https://images-eu.ssl-images-amazon.com/images/I/4155slrltML._SX95_.jpg" ' \
                b'alt="Je ne reverrai plus le monde : Textes de prison par Altan" title="Je ne reverrai plus le monde ' \
                b': Textes de prison par Altan" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Je ' \
                b'ne reverrai plus le monde : Textes de prison</h2></a><a href="/auteur/Ahmet-Altan/251947"><h3 ' \
                b'style="margin:0px">Ahmet Altan</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/McGee-American-Royals/1167325" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/41hcpiKyV1L._SX95_.jpg" alt="American Royals ' \
                b'par McGee" title="American Royals par McGee" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">American Royals</h2></a><a href="/auteur/Katharine-McGee/418210"><h3 ' \
                b'style="margin:0px">Katharine McGee</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Puertolas-La-police-des-fleurs-des-arbres-et-des-forets/1158127" ><img loading="lazy" ' \
                b'src=\n\t\t  \t\t  \t"https://images-eu.ssl-images-amazon.com/images/I/51mLj8vmAfL._SX95_.jpg" ' \
                b'alt="La police des fleurs, des arbres et des for\xeats par Pu\xe9rtolas" title="La police des ' \
                b'fleurs, des arbres et des for\xeats par Pu\xe9rtolas" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">La ' \
                b'police des fleurs, des arbres et des for\xeats</h2></a><a ' \
                b'href="/auteur/Romain-Puertolas/274608"><h3 style="margin:0px">Romain Pu\xe9rtolas</h3></a><h3 ' \
                b'style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" style="max-width:120px;" ' \
                b'itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Trebor-Combien-de-pas-jusqua-la-lune-/1161604" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/41ahuJtehpL._SX95_.jpg" alt="Combien de pas ' \
                b'jusqu\'\xe0 la lune ? par Trebor" title="Combien de pas jusqu\'\xe0 la lune ? par Trebor" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Combien de pas jusqu\'\xe0 la lune ?</h2></a><a href="/auteur/Carole-Trebor/201599"><h3 ' \
                b'style="margin:0px">Carole Trebor</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Desmurget-La-fabrique-du-cretin-digital/1167636" ><img loading="lazy" src=\n\t\t  ' \
                b'\t\t  \t"https://images-eu.ssl-images-amazon.com/images/I/41SJaY%2BbfsL._SX95_.jpg" alt="La ' \
                b'fabrique du cr\xe9tin digital par Desmurget" title="La fabrique du cr\xe9tin digital par Desmurget" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">La ' \
                b'fabrique du cr\xe9tin digital</h2></a><a href="/auteur/Michel-Desmurget/124235"><h3 ' \
                b'style="margin:0px">Michel Desmurget</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Rivault-Collisions/1155661" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/51i87p6eciL._SX95_.jpg" alt="Collisions par ' \
                b'Rivault" title="Collisions par Rivault" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Collisions</h2></a><a href="/auteur/Alexandra-Rivault/513975"><h3 ' \
                b'style="margin:0px">Alexandra Rivault</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div ' \
                b'class="col col-2-4 list_livre" style="max-width:120px;" itemscope ' \
                b'itemtype="http://schema.org/Book"><a href="/livres/Rivers-Lincivilite-des-fantomes/1158277" ><img ' \
                b'loading="lazy" src=\n\t\t  \t\t  \t"/couv/cvt_Lincivilite-des-fantomes_5580.jpg" ' \
                b'alt="L\'incivilit\xe9 des fant\xf4mes par Rivers" title="L\'incivilit\xe9 des fant\xf4mes par ' \
                b'Rivers" onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">L\'incivilit\xe9 des fant\xf4mes</h2></a><a href="/auteur/Solomon-Rivers/514938"><h3 ' \
                b'style="margin:0px">Solomon Rivers</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Dorison-Le-chateau-des-animaux-tome-1--Miss-Bengalore/1159801" ><img loading="lazy" ' \
                b'src=\n\t\t  \t\t  \t"/couv/cvt_Le-chateau-des-animaux-Tome-1--Miss-Bengalore_8857.jpg" alt="Le ' \
                b'ch\xe2teau des animaux, tome 1 : Miss Bengalore par Dorison" title="Le ch\xe2teau des animaux, ' \
                b'tome 1 : Miss Bengalore par Dorison" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Le ' \
                b'ch\xe2teau des animaux, tome 1 : Miss Bengalore</h2></a><a href="/auteur/Xavier-Dorison/2412"><h3 ' \
                b'style="margin:0px">Xavier Dorison</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Giraud-Jour-de-courage/1149680" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/418QsAaak6L._SX95_.jpg" alt="Jour de courage ' \
                b'par Giraud" title="Jour de courage par Giraud" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Jour de courage</h2></a><a href="/auteur/Brigitte-Giraud/3643"><h3 ' \
                b'style="margin:0px">Brigitte Giraud</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Bryndza-Liquide-inflammable/1155379" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"/couv/cvt_Liquide-inflammable_9319.jpg" alt="Liquide inflammable par Bryndza" title="Liquide ' \
                b'inflammable par Bryndza" onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 ' \
                b'itemprop="name" style="">Liquide inflammable</h2></a><a href="/auteur/Robert-Bryndza/442713"><h3 ' \
                b'style="margin:0px">Robert Bryndza</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Nicholls-Evelyn-May-et-Nell--Pour-un-monde-plus-juste/1162827" ><img loading="lazy" ' \
                b'src=\n\t\t  \t\t  \t"https://images-eu.ssl-images-amazon.com/images/I/41CzBrwJPhL._SX95_.jpg" ' \
                b'alt="Evelyn, May et Nell : Pour un monde plus juste par Nicholls" title="Evelyn, May et Nell : Pour ' \
                b'un monde plus juste par Nicholls" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Evelyn, May et Nell : Pour un monde plus juste</h2></a><a ' \
                b'href="/auteur/Sally-Nicholls/55019"><h3 style="margin:0px">Sally Nicholls</h3></a><h3 ' \
                b'style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" style="max-width:120px;" ' \
                b'itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Appanah-Le-ciel-par-dessus-le-toit/1147789" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/41fvtDi1VuL._SX95_.jpg" alt="Le ciel par-dessus ' \
                b'le toit par Appanah" title="Le ciel par-dessus le toit par Appanah" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Le ' \
                b'ciel par-dessus le toit</h2></a><a href="/auteur/Nathacha-Appanah/19917"><h3 ' \
                b'style="margin:0px">Nathacha Appanah</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Lee-Destins-troubles/1161498" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"/couv/cvt_Destins-Troubles_5925.jpg" alt="Destins troubles par Lee" title="Destins troubles par ' \
                b'Lee" onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Destins troubles</h2></a><a href="/auteur/Geneva-Lee/393817"><h3 style="margin:0px">Geneva ' \
                b'Lee</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" ' \
                b'style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Seksik-Chaplin-en-Amerique/1166883" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"/couv/cvt_Chaplin-en-Amerique_3882.jpg" alt="Chaplin en Am\xe9rique par Seksik" title="Chaplin ' \
                b'en Am\xe9rique par Seksik" onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 ' \
                b'itemprop="name" style="">Chaplin en Am\xe9rique</h2></a><a href="/auteur/Laurent-Seksik/67705"><h3 ' \
                b'style="margin:0px">Laurent Seksik</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Cece-Les-bizarres-tome-1-Catastrophe-a-Huggabie-City/1164667" ><img loading="lazy" ' \
                b'src=\n\t\t  \t\t  \t"https://images-eu.ssl-images-amazon.com/images/I/51ryXRp0QCL._SX95_.jpg" ' \
                b'alt="Les bizarres, tome 1: Catastrophe \xe0 Huggabie City par Cece" title="Les bizarres, ' \
                b'tome 1: Catastrophe \xe0 Huggabie City par Cece" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Les ' \
                b'bizarres, tome 1: Catastrophe \xe0 Huggabie City</h2></a><a href="/auteur/Adam-Cece/517700"><h3 ' \
                b'style="margin:0px">Adam Cece</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Arnaud-Maintenant-comme-avant/1150446" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/41yqKgOmzrL._SX95_.jpg" alt="Maintenant, ' \
                b'comme avant par Arnaud" title="Maintenant, comme avant par Arnaud" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Maintenant, comme avant</h2></a><a href="/auteur/Juliette-Arnaud/236708"><h3 ' \
                b'style="margin:0px">Juliette Arnaud</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Lopez-Barbarossa--1941-La-guerre-absolue/1168655" ><img loading="lazy" src=\n\t\t  ' \
                b'\t\t  \t"https://images-eu.ssl-images-amazon.com/images/I/41azCGhDrQL._SX95_.jpg" alt="Barbarossa : ' \
                b'1941. La guerre absolue par Lopez" title="Barbarossa : 1941. La guerre absolue par Lopez" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" ' \
                b'style="">Barbarossa : 1941. La guerre absolue</h2></a><a href="/auteur/Jean-Lopez/87962"><h3 ' \
                b'style="margin:0px">Jean Lopez</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col ' \
                b'col-2-4 list_livre" style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Bortoli-Too-Young/1167003" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"https://images-eu.ssl-images-amazon.com/images/I/414AEj48y6L._SX95_.jpg" alt="Too Young par ' \
                b'Bortoli" title="Too Young par Bortoli" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Too ' \
                b'Young</h2></a><a href="/auteur/Margot-D-Bortoli/483655"><h3 style="margin:0px">Margot D. ' \
                b'Bortoli</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div class="col col-2-4 list_livre" ' \
                b'style="max-width:120px;" itemscope itemtype="http://schema.org/Book"><a ' \
                b'href="/livres/Rebillard-Les-seins-de-Fatima-tome-1/1169110" ><img loading="lazy" src=\n\t\t  \t\t  ' \
                b'\t"/couv/cvt_Les-seins-de-Fatima-Tome-1_8934.jpg" alt="Les seins de Fatima, tome 1 par Rebillard" ' \
                b'title="Les seins de Fatima, tome 1 par Rebillard" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">Les ' \
                b'seins de Fatima, tome 1</h2></a><a href="/auteur/Georges-Daniel-Rebillard/456011"><h3 ' \
                b'style="margin:0px">Georges Daniel Rebillard</h3></a><h3 style="margin:5px 0 0 0"></h3></div><div ' \
                b'class="col col-2-4 list_livre" style="max-width:120px;" itemscope ' \
                b'itemtype="http://schema.org/Book"><a href="/livres/Seveno-6-ans/1167496" ><img loading="lazy" ' \
                b'src=\n\t\t  \t\t  \t"https://images-eu.ssl-images-amazon.com/images/I/41CNOhHRe8L._SX95_.jpg" ' \
                b'alt="6 ans par Seveno" title="6 ans par Seveno" ' \
                b'onError="this.onerror=null;this.src=\'/images/couv-defaut.jpg\';"/><h2 itemprop="name" style="">6 ' \
                b'ans</h2></a><a href="/auteur/Elle-Seveno/420191"><h3 style="margin:0px">Elle Seveno</h3></a><h3 ' \
                b'style="margin:5px 0 0 0"></h3></div></div><div class="gris side_l_content">\n\tLa note est obtenue ' \
                b'par l\'estimateur de Bayes : (nb_notes/(nb_notes+min))*M + (min/(nb_notes+min))*C \n\t<br>\navec M ' \
                b'= Moyenne des notes <br>\nmin = nombre minimal de votes requis (50) <br>\nC = moyenne des notes sur ' \
                b'Babelio (3.8079)<br></div><div id=\'div-gpt-ad-1442725480364-0\' ' \
                b'style="z-index:5;margin:auto;text-align:center;position:relative;"><script ' \
                b'type=\'text/javascript\'>\ngoogletag.cmd.push(function() { googletag.display(' \
                b'\'div-gpt-ad-1442725480364-0\'); });\n</script></div></div><footer style="height:100%"><div ' \
                b'class="col-4 col footer_col"><div class="footer_titre">Navigation</div><a href="/aide.php" ' \
                b'rel="nofollow" class="col col-4 footer_links">Aide</a><a href="/partenariats_editeurs.php" ' \
                b'rel="nofollow" class="col col-4 footer_links">Publicit\xe9</a><a href="/massecritique.php" ' \
                b'class="col col-4 footer_links">Masse critique</a><a ' \
                b'href="https://babelio.freshdesk.com/support/home" rel="nofollow" target="_blank" class="col col-4 ' \
                b'footer_links">Contact</a><a href="https://www.babeltheque.fr" target="blank" rel="nofollow" ' \
                b'class="col col-4 footer_links" target="_blank">Babelth\xe8que</a><a href="/partenaires.php" ' \
                b'rel="nofollow" class="col col-4 footer_links">Sites Partenaires</a><a ' \
                b'href="https://babelio.wordpress.com/" class="col col-4 footer_links" target="_blank">Blog</a><a ' \
                b'href="/apropos.php" rel="nofollow" class="col col-4 footer_links">A propos</a><a ' \
                b'href="http://www.cinetrafic.fr" target="_blank" rel="nofollow" class="col col-4 ' \
                b'footer_links">Listes de films</a><a href="https://sites.google.com/site/defibabelio/home" ' \
                b'target="_blank" rel="nofollow" class="col col-4 footer_links">D\xe9fi Babelio</a></div><div ' \
                b'class="col-4 col footer_col"><div class="footer_titre" align="center">Suivez-nous</div><div ' \
                b'class="footer_icons"><a href="/rss_critiques.php" rel="nofollow" target="_blank" target="_blank" ' \
                b'class="icon-blogger"></a><a href="https://www.facebook.com/pages/Babeliocom/36218830677" ' \
                b'rel="nofollow" target="_blank" class="icon-blogger4"></a><a href="https://www.twitter.com/babelio" ' \
                b'class="icon-blogger5" target="_blank"></a></div></div><div class="col-4 col footer_col" ><a ' \
                b'href="https://itunes.apple.com/fr/app/babelio/id1449315145?l=fr&ls=1&mt=8" target="_blank" ' \
                b'rel="nofolliow"><img src="/images/relais_app_apple.png"></a><a ' \
                b'href="https://play.google.com/store/apps/details?id=com.babelio.babelio&hl=fr" target="_blank" ' \
                b'rel="nofolliow"><img src="/images/relais_app_google.png"></a></div><div class="col-4 col ' \
                b'footer_col" ><div id="back-to-top" >retour en haut de page\n         \n        <svg viewBox="0 0 ' \
                b'595.279 841.891"><path fill="none" d="M581.864,500.842l-31.371,31.371l-249.81-249.839L49.832,' \
                b'532.213L18.52,504.752l283.672-278.441\n            L581.864,500.842z"/></svg></div></div><div ' \
                b'class="footer_bottom row"><div>\xa9 BABELIO - ' \
                b'2007-2018</div><span></span></div></footer></div></div><script src="/js_cache/1,34,noJQ,35,36,37,' \
                b'38,39,48,47,__45" type="text/javascript" charset="ISO-8859-1"></script><div ' \
                b'class="cookie-message"><p class="cookie-content">Les cookies assurent le bon fonctionnement de ' \
                b'Babelio. En poursuivant la navigation vous en acceptez le fonctionnement \n    &nbsp; <button ' \
                b'class="btn inline" style="padding: 8px;" id="cookie_ok">OK</button> &nbsp; <nobr><a ' \
                b'href="/cookies.php" style="color: #FF9D38" target="_blank">En savoir ' \
                b'plus</a></nobr></p></div><script type="text/javascript" language="javascript">\n$("#cookie_ok").on(' \
                b'\'click\', function(){//#cookie stop is the Id of the button that closes the disclaimer\n\t\n\t    ' \
                b'$(\'.cookie-message\').slideUp(); //#cookie_disclaimer is the Id of the cookie disclaimer ' \
                b'container\n\n   //here i set the cookie variable "disclaimer" to true so i can check if the user \n ' \
                b'      var nDays = 999; \n       var cookieName = "disclaimer";\n       var cookieValue = ' \
                b'"true";\n\n       var today = new Date();\n       var expire = new Date();\n       expire.setTime(' \
                b'today.getTime() + 3600000*24*nDays);\n       document.cookie = cookieName+"="+escape(' \
                b'cookieValue)+";expires="+expire.toGMTString()+";path=/";\n  //done with the cookie\n });\n ' \
                b'</script><style media="screen" type="text/css">\n.cookie-message {\n      width: 100%;\n      ' \
                b'background: #333;\n      color: #ddd;\n      padding: .15em;\n      position: fixed;\n      ' \
                b'text-align:center;\n      bottom: 0;\n      z-index: 99999;\n      }\n.cookie-content {' \
                b'\n\tmargin:auto;\n\ttext-align:center;\n}\n      \n</style><script language=javascript> \n\n\n\n    ' \
                b'$(\'.rateit\').bind(\'rated reset\', function (e) {\n         var ri = $(this);\n\n\n         var ' \
                b'value = ri.rateit(\'value\');\n         var ID = ri.data(\'id\');\n         var table = ri.data(' \
                b'\'table\');\n         var sel= ri.data(\'sel\');\n         //alert(value + \' \' + ID + \' \' + ' \
                b'table );\n\n\n\n         //maybe we want to disable voting?\n         //ri.rateit(\'readonly\', ' \
                b'true);\n \n         $.ajax({\n             url: \'/aj_rating_rpc.php\', //your server side script\n ' \
                b'            data: { id: ID, value: value, table: table,sel: sel }, //our data\n             type: ' \
                b'\'POST\',\n             success: function (data) {\n                 //$(\'#response\').append(' \
                b'\'<li>\' + data + \'</li>\');\n                 //alert(data);\n\n             },\n             ' \
                b'error: function (jxhr, msg, err) {\n\n             }\n         });\n \n     ' \
                b'});\n</script><span></span><script>\n(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[' \
                b'r]=i[r]||function(){\n(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(' \
                b'o),\nm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n})(window,' \
                b'document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\nga(\'create\', ' \
                b'\'UA-1000343-2\', \'auto\');  // Replace with your property ID.\nga(\'send\', ' \
                b'\'pageview\');\n\n</script></body></html>.. .. '
